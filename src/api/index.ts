import { ApolloClient, InMemoryCache } from '@apollo/client';

export const client = new ApolloClient({
  uri: 'https://graphql.simplywall.st/graphql',
  cache: new InMemoryCache(),
});
