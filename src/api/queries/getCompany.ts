import { gql } from '@apollo/client';
import { client } from '..';

const QUERY = gql`
  query ($symbol: String!) {
    Company(symbol: $symbol) {
      name
      uniqueSymbol
      sector {
        name
      }
      analysisValue {
        marketCapUSD
      }
      score {
        health
        value
        past
        future
        dividend
      }
    }
  }
`;

export type Company = {
  readonly name: string;
  readonly uniqueSymbol: string;
  readonly analysisValue: {
    readonly marketCapUSD: number;
  };
  readonly sector: {
    readonly name: string;
  };
  readonly score: {
    readonly health: number;
    readonly value: number;
    readonly past: number;
    readonly future: number;
    readonly dividend: number;
  };
};

export type Response = {
  readonly Company: Company;
};

export async function getCompany(symbol: string): Promise<Response['Company']> {
  const { data } = await client.query<Response>({
    query: QUERY,
    variables: { symbol },
  });

  return data.Company;
}
