import { BotType } from './enums';
import { PerkID } from './simulation/perks';

export type BotStats = {
  readonly durability: number;
  readonly damage: number;
  readonly clipSize: number;
  readonly fireCooldownMs: number;
  readonly reloadCooldownMs: number;
  readonly accuracyAngle: number;
  readonly hitboxRadius: number;
  readonly movementSpeed: number;
};

export type BotDefinition = {
  readonly id: string;
  readonly power: number;
  readonly type: BotType;
  readonly name: string;
  readonly symbol: string;
  readonly stats: BotStats;
  readonly availablePerks: readonly PerkID[];
  readonly appliedPerks: readonly PerkID[];
};

export type TeamDefinition = {
  readonly id: string;
  readonly name: string;
  readonly color: number;
  readonly bots: readonly BotDefinition[];
};

export type EncodedTeamDefinition = {
  readonly version: string;
  readonly data: TeamDefinition;
};
