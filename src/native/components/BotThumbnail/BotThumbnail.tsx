import { Application } from 'pixi.js';
import { FC, useEffect, useRef } from 'react';
import { BotGraphics } from '../../../simulation/engine/graphics/botGraphics';
import { createStars } from '../../../simulation/engine/world/stars/utils';
import { BotDefinition, TeamDefinition } from '../../../types';

type Props = {
  readonly team: TeamDefinition;
  readonly bot: BotDefinition;
  readonly size?: number;
};

export const BotThumbnail: FC<Props> = ({ team, bot, size = 96 }) => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const app = useRef<Application | null>(null);

  useEffect(() => {
    const canvas = canvasRef.current;

    if (canvas && !app.current) {
      app.current = new Application({
        view: canvas,
        backgroundColor: 0x323649,
        width: size,
        height: size,
        antialias: true,
      });

      const graphics = new BotGraphics(team.color, bot);
      const stars = createStars(50, canvas.width, canvas.height);

      graphics.position.set(canvas.width / 2, canvas.height / 2);
      graphics.rotation = Math.random() * Math.PI * 2;

      app.current.stage.addChild(...stars);
      app.current.stage.addChild(graphics);

      app.current.ticker.add(() => {
        for (const star of stars) {
          star.position.x += star.velocity.x;
          star.position.y += star.velocity.y;

          if (star.position.x > canvas.width + 10) star.position.x = -10;
          if (star.position.y > canvas.height + 10) star.position.y = -10;
        }

        graphics.rotation += 0.005;
      });
    }

    return () => {
      if (app.current) {
        app.current.destroy(false, true);
        app.current = null;
      }
    };
  }, [team.color]);

  return (
    <figure className="shadow-md overflow-hidden rounded-md border border-ui-light">
      <canvas ref={canvasRef} />
    </figure>
  );
};
