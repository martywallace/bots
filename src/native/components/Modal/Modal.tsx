import classNames from 'classnames';
import { FC, ReactElement } from 'react';
import Overlay from '../Overlay';

type Props = {
  readonly title?: string;
  readonly actions?: ReactElement;
  readonly size?: 'sm' | 'md' | 'lg';
};

const theme = (size: Props['size']) =>
  classNames(
    'flex flex-col bg-ui w-full max-w-4xl max-h-full mx-auto shadow-lg rounded-md',
    {
      'max-w-4xl': size === 'lg',
      'max-w-xl': size === 'md',
      'max-w-md': size === 'sm',
    },
  );

export const Modal: FC<Props> = ({ title, actions, children, size = 'sm' }) => {
  return (
    <Overlay>
      <div className="flex items-center h-screen overflow-hidden p-5">
        <section className={theme(size)}>
          {title && (
            <div className="border-b border-ui-lighter p-5">
              <h4 className="text-lg font-bold">{title}</h4>
            </div>
          )}

          <div className="flex-grow overflow-auto shadow-inner">{children}</div>

          {actions && (
            <div className="border-t border-ui-lighter p-5">{actions}</div>
          )}
        </section>
      </div>
    </Overlay>
  );
};
