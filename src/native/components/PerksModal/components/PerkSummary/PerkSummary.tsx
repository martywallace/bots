import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC } from 'react';
import { Perk } from '../../../../../simulation/perks';

type Props = {
  readonly perk: Perk;
  readonly available?: boolean;
};

export const PerkSummary: FC<Props> = ({ perk, available }) => {
  return (
    <div className="flex p-5 h-full rounded-md bg-ui-dark">
      <div className="flex-shrink-0">
        <FontAwesomeIcon icon={perk.metadata.icon} />
      </div>
      <div className="flex flex-col flex-grow pl-3">
        <div className="flex-grow">
          <h6 className="font-bold">{perk.metadata.name}</h6>
          <p className="text-sm opacity-70">
            {perk.metadata.description(perk)}
          </p>
        </div>
        <div className="pt-3 text-ui-lightest">
          <div className="text-xs opacity-50">Requirements</div>
          {available === undefined ? (
            <p className="text-sm">{perk.metadata.reason}</p>
          ) : available ? (
            <p className="text-sm text-green-300">
              <FontAwesomeIcon icon={faCheck} /> {perk.metadata.reason}
            </p>
          ) : (
            <p className="text-sm text-bad">
              <FontAwesomeIcon icon={faTimes} /> {perk.metadata.reason}
            </p>
          )}
        </div>
      </div>
    </div>
  );
};
