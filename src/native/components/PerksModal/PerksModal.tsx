import { FC } from 'react';
import { getAllPerks, Perk } from '../../../simulation/perks';
import Modal from '../Modal';
import PerkSummary from './components/PerkSummary';

type Props = {
  readonly onClose: () => void;
  readonly available?: readonly Perk[];
};

export const PerksModal: FC<Props> = ({ onClose, available }) => {
  return (
    <Modal
      size="lg"
      title="All Perks"
      actions={
        <button
          onClick={() => onClose()}
          className="block w-full max-w-sm mx-auto px-5 py-2 font-bold text-white bg-ui-darker hover:bg-ui-darkest rounded-md"
        >
          OK
        </button>
      }
    >
      <div className="grid grid-cols-3 gap-2 p-2">
        {getAllPerks().map((perk) => (
          <div key={perk.id}>
            <PerkSummary
              available={available && available.includes(perk)}
              perk={perk}
            />
          </div>
        ))}
      </div>
    </Modal>
  );
};
