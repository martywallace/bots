import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC, ReactNode } from 'react';

type Props = {
  readonly placeholder?: string;
  readonly value: string;
  readonly loading?: boolean;
  readonly onChange: (value: string) => void;
};

export const TextField: FC<Props> = ({
  placeholder,
  value,
  loading,
  onChange,
}) => {
  return (
    <div>
      <div className="relative">
        <input
          type="text"
          value={value}
          placeholder={placeholder}
          onChange={(event) => onChange(event.target.value)}
          className="bg-ui-darker shadow-sm border border-ui-light rounded focus:border-ui-lighter placeholder:text-ui focus:outline-none px-4 py-3 w-full"
        />

        {loading && (
          <span className="flex items-center px-4 absolute pointer-events-none inset-0 left-auto">
            <FontAwesomeIcon
              className="text-ui-lightest animate-spin"
              icon={faSpinner}
            />
          </span>
        )}
      </div>
    </div>
  );
};
