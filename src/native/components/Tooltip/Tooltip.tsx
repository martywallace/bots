import classNames from 'classnames';
import { FC, ReactNode } from 'react';

type Props = {
  readonly tip: ReactNode;
  readonly position?: 'above' | 'below';
};

const theme = (position?: Props['position']) =>
  classNames(
    'absolute z-10 left-0 mt-1 hidden group-hover:block w-64 text-sm p-4 bg-ui-lighter text-white text-left rounded shadow-lg',
    {
      'top-full mt-1': position === 'below',
      'bottom-full mb-1': position === 'above',
    },
  );

export const Tooltip: FC<Props> = ({ children, position = 'below', tip }) => {
  return (
    <div className="inline relative group">
      {children}
      <div className={theme(position)}>{tip}</div>
    </div>
  );
};
