import { FC } from 'react';
import { getAccuracyPercentage } from '../../../simulation/generator/utils';
import { applyPerkStatAdjustments, Perk } from '../../../simulation/perks';
import { BotDefinition } from '../../../types';
import BotStat from './components/BotStat';

type Props = {
  readonly bot: BotDefinition;
  readonly perks: readonly Perk[];
};

export const BotStats: FC<Props> = ({ bot, perks }) => {
  const adjusted = applyPerkStatAdjustments(bot.stats, perks);

  return (
    <>
      <BotStat
        name="Durability"
        value={bot.stats.durability}
        adjusted={adjusted.durability}
        formatter={(value) => value.toString()}
        tip={
          <p>
            The amount of damage this bot can take before it is destroyed. The
            health score of the company factors into calculating this value.
          </p>
        }
      />
      <BotStat
        name="Damage"
        value={bot.stats.damage}
        adjusted={adjusted.damage}
        formatter={(value) => value.toString()}
        tip={
          <p>
            The amount of damage this bot can deliver through typical attacks.
          </p>
        }
      />
      <BotStat
        name="Ammo"
        adjusted={adjusted.clipSize}
        value={bot.stats.clipSize}
        formatter={(value) => value.toString()}
        tip={
          <p>
            The amount of projectiles the bot can fire before needing to reload.
          </p>
        }
      />
      <BotStat
        name="Reload Delay"
        value={bot.stats.reloadCooldownMs}
        adjusted={adjusted.reloadCooldownMs}
        formatter={(value) => (value / 1000).toFixed(2) + 's'}
        reverse
        tip={
          <p>
            The amount of time it takes the bot to reload once all its ammo has
            been depleated.
          </p>
        }
      />
      <BotStat
        name="Fire Delay"
        value={bot.stats.fireCooldownMs}
        adjusted={adjusted.fireCooldownMs}
        reverse
        formatter={(value) => (value / 1000).toFixed(2) + 's'}
        tip={<p>The amount of time between firing projectiles.</p>}
      />
      <BotStat
        name="Accuracy"
        value={bot.stats.accuracyAngle}
        adjusted={adjusted.accuracyAngle}
        reverse
        reverseSign
        formatter={(value) =>
          (getAccuracyPercentage(value) * 100).toFixed(1) + '%'
        }
        adjustmentFomatter={(value) =>
          (Math.round((value / (Math.PI / 2)) * 1000) / 10).toFixed(1) + '%'
        }
        tip={
          <p>
            The accuracy of projectiles launched from this bot towards its
            target.
          </p>
        }
      />
    </>
  );
};
