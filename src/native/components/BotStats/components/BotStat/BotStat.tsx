import {
  faCaretDown,
  faCaretUp,
  faQuestionCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import { FC, ReactNode } from 'react';
import Tooltip from '../../../Tooltip';

type Props = {
  readonly name: string;
  readonly tip: ReactNode;
  readonly value: number;
  readonly adjusted: number;
  readonly reverse?: boolean;
  readonly reverseSign?: boolean;
  readonly formatter: (value: number) => string;
  readonly adjustmentFomatter?: (value: number) => string;
};

const isGood = (value: number, reverse?: boolean) => {
  if ((value > 0 && !reverse) || (value < 0 && reverse)) return true;
  return false;
};

const adjustmentTheme = (value: number, reverse?: boolean) =>
  classNames({
    'text-green-300': isGood(value, reverse),
    'text-red-300': !isGood(value, reverse),
  });

export const BotStat: FC<Props> = ({
  name,
  tip,
  value,
  adjusted,
  reverse,
  reverseSign,
  formatter,
  adjustmentFomatter,
}) => {
  const diff = adjusted - value;

  return (
    <div className="grid grid-cols-2">
      <div>
        {name}{' '}
        <Tooltip tip={tip}>
          <FontAwesomeIcon className="text-ui-light" icon={faQuestionCircle} />
        </Tooltip>
      </div>
      <div className="text-right">
        {formatter(value)}
        {!!diff && (
          <span className={adjustmentTheme(diff, reverse)}>
            {' '}
            {isGood(diff, reverseSign) ? (
              <FontAwesomeIcon icon={faCaretUp} />
            ) : (
              <FontAwesomeIcon icon={faCaretDown} />
            )}{' '}
            {adjustmentFomatter
              ? adjustmentFomatter(Math.abs(diff))
              : formatter(Math.abs(diff))}
          </span>
        )}
      </div>
    </div>
  );
};
