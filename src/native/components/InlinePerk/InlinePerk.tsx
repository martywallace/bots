import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC } from 'react';
import { Perk } from '../../../simulation/perks';
import Tooltip from '../Tooltip';

type Props = {
  readonly perk: Perk;
};

export const InlinePerk: FC<Props> = ({ perk }) => {
  return (
    <Tooltip position="above" tip={<p>{perk.metadata.description(perk)}</p>}>
      <span className="inline-block cursor-help font-bold">
        <FontAwesomeIcon
          className="text-ui-lightest text-xs align-middle"
          icon={perk.metadata.icon}
        />{' '}
        <span className="align-middle">{perk.metadata.name}</span>
      </span>
    </Tooltip>
  );
};
