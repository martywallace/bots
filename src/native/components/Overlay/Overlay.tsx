import { FC } from 'react';
import OverlayMount from '../OverlayMount';

export const Overlay: FC = ({ children }) => {
  return (
    <OverlayMount>
      <div className="bg-ui-darkest bg-opacity-80 fixed inset-0">
        {children}
      </div>
    </OverlayMount>
  );
};
