import classNames from 'classnames';
import { FC } from 'react';

type Props = {
  readonly style?: 'good' | 'bad';
  readonly disabled?: boolean;
  readonly onClick: () => void;
};

const theme = ({ disabled, style }: Partial<Props>) =>
  classNames(
    'flex items-center text-left shadow-md px-4 py-3 rounded-sm rounded',
    {
      'bg-bad text-white': style === 'bad',
      'bg-good text-white': style === 'good',
    },
  );

export const Button: FC<Props> = ({ children, style, disabled, onClick }) => {
  return (
    <button
      disabled={disabled}
      className={theme({ disabled, style })}
      onClick={(event) => {
        event.preventDefault();
        onClick();
      }}
    >
      {children}
    </button>
  );
};
