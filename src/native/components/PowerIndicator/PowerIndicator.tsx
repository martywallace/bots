import { FC } from 'react';
import Tooltip from '../Tooltip';

type Props = {
  readonly value: number;
  readonly short?: boolean;
};

export const PowerIndicator: FC<Props> = ({ value, short }) => {
  return (
    <Tooltip
      tip={
        <>
          <p>
            Power level is derived from market cap and defines the baseline
            competence of a bot. Power level counts towards team quota.
          </p>
        </>
      }
    >
      <span className="inline-block px-3 py-1 rounded-sm shadow-md bg-white text-ui-dark font-bold text-sm">
        {value.toFixed(1)}
        {!short && <> Power</>}
      </span>
    </Tooltip>
  );
};
