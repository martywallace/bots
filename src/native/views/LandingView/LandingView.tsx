import { faRocket } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useLocalStorage from '@rehooks/local-storage';
import { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LS_SEED, LS_TEAMS } from '../../../constants';
import { Random } from '../../../simulation/engine/utils/random';
import { TeamDefinition } from '../../../types';
import PerksModal from '../../components/PerksModal';
import Tooltip from '../../components/Tooltip';
import Teams from './components/Teams';

export const LandingView: FC = () => {
  const navigate = useNavigate();

  const [showPerksModal, setShowPerksModal] = useState(false);
  const [seed, setSeed] = useLocalStorage(LS_SEED, Random.id(8));
  const [teams] = useLocalStorage<readonly TeamDefinition[]>(LS_TEAMS, []);

  const handleLaunch = () => {
    navigate(`/battle`);
  };

  return (
    <>
      <div className="min-h-screen w-full flex items-center">
        <main className="px-10 py-20 w-full">
          <div className="max-w-screen-md mx-auto text-center">
            <h1 className="text-4xl mb-10 font-black">stock bots.</h1>

            <div className="prose max-w-md mx-auto text-sm">
              <p>
                Build teams using your favourite stocks and pit them against
                each other in the arena. Personalise your bots using{' '}
                <button
                  onClick={() => setShowPerksModal(true)}
                  className="text-white opacity-75 hover:opacity-100 underline"
                >
                  perks
                </button>
                , which are available based on traits of the underlying company.
              </p>
            </div>
          </div>

          <div className="max-w-screen-lg mx-auto my-10">
            <Teams />
          </div>

          <div className="max-w-lg mx-auto">
            <div className="grid gap-5 grid-cols-2">
              <div>
                <Tooltip
                  position="above"
                  tip={
                    <p>
                      Seeds the random number generation to produce similar
                      simulations.
                    </p>
                  }
                >
                  <input
                    type="text"
                    placeholder="Seed"
                    value={seed}
                    onChange={(event) => setSeed(event.target.value)}
                    className="px-4 py-3 block w-full bg-ui-darker text-ui-lightest focus:text-white rounded-md focus:outline-none focus:bg-ui-darkest"
                  />
                </Tooltip>
              </div>
              <div>
                <button
                  className="bg-primary hover:bg-white h-full disabled:bg-ui-darker disabled:cursor-not-allowed shadow-md disabled:shadow-none disabled:text-ui-lighter text-ui-dark font-bold text-center w-full block mx-auto rounded"
                  disabled={teams.length < 2}
                  onClick={() => handleLaunch()}
                >
                  <span className="mr-4">Launch</span>
                  <FontAwesomeIcon icon={faRocket} />
                </button>
              </div>
            </div>
          </div>
        </main>
      </div>

      {showPerksModal && (
        <PerksModal onClose={() => setShowPerksModal(false)} />
      )}
    </>
  );
};
