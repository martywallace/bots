import { faFileImport, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useLocalStorage from '@rehooks/local-storage';
import { FC, useState } from 'react';
import { LS_TEAMS } from '../../../../../constants';
import { Random } from '../../../../../simulation/engine/utils/random';
import { TeamDefinition } from '../../../../../types';
import Tooltip from '../../../../components/Tooltip';
import ImportModal from '../ImportModal';
import { TeamBlock } from './components/TeamBlock/TeamBlock';
import TeamBuilder from './components/TeamBuilder';

export const Teams: FC = () => {
  const [showImportModal, setShowImportModal] = useState(false);

  const [teams, setTeams] = useLocalStorage<readonly TeamDefinition[]>(
    LS_TEAMS,
    [],
  );

  const [selectedTeamIndex, setSelectedTeamIndex] = useState<number | null>(
    null,
  );

  const createNewTeam = () => {
    const team: TeamDefinition = {
      id: Random.id(),
      name: '',
      color: Math.round(Random.get() * 0xffffff),
      bots: [],
    };

    setTeams(teams.concat(team));
    setSelectedTeamIndex(teams.length);
  };

  const updateTeam = (index: number, value: Partial<TeamDefinition>) => {
    setTeams(
      teams.map((team, i) => (i === index ? { ...team, ...value } : team)),
    );
  };

  const handleRemoveTeam = (index: number) => {
    setTeams(teams.filter((_, i) => i !== index));
  };

  const handleImportTeam = (team: TeamDefinition) => {
    setShowImportModal(false);
    setTeams(teams.concat(team));
  };

  const handleCancel = (id: string) => {
    setSelectedTeamIndex(null);
    setTeams(teams.filter((team) => team.id !== id));
  };

  return (
    <>
      <section className="bg-ui-darker rounded p-10">
        <div className="grid gap-5 grid-cols-3">
          {teams.map((team, index) => (
            <div key={team.id}>
              <TeamBlock
                definition={team}
                onView={() => setSelectedTeamIndex(index)}
                onRemove={() => handleRemoveTeam(index)}
              />
            </div>
          ))}

          <div className="bg-ui-darker text-ui-lighter p-2 border border-ui-dark shadow rounded-sm text-center flex items-center h-full">
            <div className="grid h-full grid-cols-2 gap-2 w-full">
              <Tooltip tip={<p>Create a new team from scratch.</p>}>
                <button
                  className="w-full py-10 rounded-md hover:shadow-lg h-full hover:bg-ui hover:text-white"
                  onClick={() => createNewTeam()}
                >
                  <FontAwesomeIcon className="text-4xl" icon={faPlus} />
                </button>
              </Tooltip>

              <Tooltip tip={<p>Import a team using import code.</p>}>
                <button
                  className="w-full py-10 rounded-md hover:shadow-lg h-full hover:bg-ui hover:text-white"
                  onClick={() => setShowImportModal(true)}
                >
                  <FontAwesomeIcon className="text-4xl" icon={faFileImport} />
                </button>
              </Tooltip>
            </div>
          </div>
        </div>
      </section>

      {selectedTeamIndex !== null && (
        <TeamBuilder
          team={teams[selectedTeamIndex]}
          onChange={(value) => updateTeam(selectedTeamIndex, value)}
          onClose={() => setSelectedTeamIndex(null)}
          onCancel={() => handleCancel(teams[selectedTeamIndex].id)}
        />
      )}

      {showImportModal && (
        <ImportModal
          onClose={() => setShowImportModal(false)}
          onImport={(team) => handleImportTeam(team)}
        />
      )}
    </>
  );
};
