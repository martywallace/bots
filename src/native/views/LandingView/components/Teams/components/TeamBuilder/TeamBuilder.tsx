import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC, useState } from 'react';
import { TEAM_MAX_QUOTA } from '../../../../../../../constants';
import { loadBot } from '../../../../../../../simulation/generator';
import { BotDefinition, TeamDefinition } from '../../../../../../../types';
import Overlay from '../../../../../../components/Overlay';
import TextField from '../../../../../../components/TextField';
import ExportModal from '../../../ExportModal';
import BotBuilder from '../BotBuilder';
import BotContainer from '../BotContainer';

type Props = {
  readonly team: TeamDefinition;
  readonly onChange: (team: Partial<TeamDefinition>) => void;
  readonly onCancel: () => void;
  readonly onClose: () => void;
};

export const TeamBuilder: FC<Props> = ({
  team,
  onChange,
  onClose,
  onCancel,
}) => {
  const [query, setQuery] = useState('');
  const [loading, setLoading] = useState(false);
  const [tentativeBot, setTentativeBot] = useState<BotDefinition | null>(null);
  const [showExportModal, setShowExportModal] = useState(false);

  const quotaConsumed = team.bots.reduce((total, bot) => total + bot.power, 0);

  const quotaPercentage = quotaConsumed / TEAM_MAX_QUOTA;

  const handleSearch = async () => {
    setLoading(true);

    try {
      setTentativeBot(await loadBot(query));
    } catch (error) {
      console.warn(`Could not produce bot from ${query}.`);
    }

    setLoading(false);
  };

  const handleAddBot = (bot: BotDefinition) => {
    onChange({ bots: team.bots.concat(bot) });
    setTentativeBot(null);
  };

  const handleRemoveBot = (bot: BotDefinition) => {
    onChange({ bots: team.bots.filter((entry) => entry.id !== bot.id) });
  };

  const handleDone = () => {
    if (team.bots.length === 0) {
      // Don't keep it handing around if no bots.
      onCancel();
    } else {
      onClose();
    }
  };

  return (
    <>
      <Overlay>
        <aside className="bg-ui-dark flex flex-col shadow-lg h-screen absolute inset-0 left-auto w-full max-w-screen-md">
          <div className="p-8 border-b border-ui-darker">
            <h2 className="font-bold text-xl">Team Builder</h2>

            <div className="pt-5 grid gap-5 grid-cols-7">
              <div>
                <h6 className="text-xs text-ui-lightest mb-1 uppercase">
                  Color
                </h6>
                <div
                  className="rounded h-3 shadow-md"
                  style={{
                    backgroundColor:
                      '#' + team.color.toString(16).padStart(6, '0'),
                  }}
                />
              </div>
              <div className="col-span-6">
                <h6 className="text-xs text-ui-lightest mb-1 uppercase">
                  Quota ({(quotaPercentage * 100).toFixed(1)}%)
                </h6>

                <div className="bg-ui-darkest rounded overflow-hidden shadow-md">
                  <div
                    className="bg-primary rounded h-3"
                    style={{ width: `${quotaPercentage * 100}%` }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col flex-grow overflow-auto p-8">
            <div className="grid mb-5 flex-shrink-0 gap-5 grid-cols-2">
              <div>
                <label className="text-ui-lighter text-xs uppercase">
                  Team Name
                </label>
                <TextField
                  placeholder="Dividend Destroyers"
                  value={team.name}
                  onChange={(value) => onChange({ name: value })}
                />
              </div>
              <div>
                <label className="text-ui-lighter text-xs uppercase">
                  Company Search
                </label>
                <div className="grid grid-cols-4 gap-1">
                  <div className="col-span-3">
                    <TextField
                      placeholder="NYSE:SQ"
                      value={query}
                      onChange={(value) => setQuery(value)}
                    />
                  </div>
                  <div>
                    <button
                      disabled={loading}
                      onClick={() => handleSearch()}
                      className="flex w-full items-center h-full bg-ui-lighter disabled:bg-ui-darker disabled:text-ui-light disabled:shadow-none hover:bg-ui-lightest text-white rounded shadow-md"
                    >
                      <span className="w-full text-center">
                        <FontAwesomeIcon icon={faSearch} />
                      </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className="relative bg-ui-darkest shadow-inner p-5 flex-grow rounded overflow-auto">
              <BotContainer
                team={team}
                onRemoveBot={(bot) => handleRemoveBot(bot)}
              />
            </div>
          </div>
          <div className="p-5 grid gap-5 grid-cols-2 border-t border-ui-darker text-center">
            <div>
              <button
                className="text-center w-full bg-ui-light hover:bg-ui-lighter hover:shadow-md font-bold rounded-md py-2 text-white"
                onClick={() => handleDone()}
              >
                Save
              </button>
            </div>
            <div>
              <button
                onClick={() => setShowExportModal(true)}
                className="text-center w-full bg-ui-light hover:bg-ui-lighter hover:shadow-md font-bold rounded-md py-2 text-white"
              >
                Export
              </button>
            </div>
          </div>
        </aside>
      </Overlay>

      {tentativeBot && (
        <BotBuilder
          team={team}
          template={tentativeBot}
          onAdd={(bot) => handleAddBot(bot)}
          onCancel={() => setTentativeBot(null)}
        />
      )}

      {showExportModal && (
        <ExportModal team={team} onClose={() => setShowExportModal(false)} />
      )}
    </>
  );
};
