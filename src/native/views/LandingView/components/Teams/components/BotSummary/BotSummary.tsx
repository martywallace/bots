import { FC, Fragment } from 'react';
import { getPerks } from '../../../../../../../simulation/perks';
import { BotDefinition, TeamDefinition } from '../../../../../../../types';
import BotThumbnail from '../../../../../../components/BotThumbnail';
import InlinePerk from '../../../../../../components/InlinePerk';
import PowerIndicator from '../../../../../../components/PowerIndicator';
import BotStats from '../../../../../../components/BotStats';

type Props = {
  readonly team: TeamDefinition;
  readonly bot: BotDefinition;
  readonly onRemove: () => void;
  readonly exceedsQuota?: boolean;
};

export const BotSummary: FC<Props> = ({
  team,
  bot,
  onRemove,
  exceedsQuota,
}) => {
  const perks = getPerks(bot.appliedPerks);

  return (
    <article className="relative h-full">
      <div className="flex flex-col h-full bg-ui-darker border border-ui rounded shadow-md">
        <div className="flex relative flex-grow p-3">
          <div className="flex-shrink-0">
            <BotThumbnail team={team} bot={bot} size={86} />
          </div>
          <div className="flex-grow pl-6">
            <span className="block text-ui-lighter text-xs">{bot.symbol}</span>

            <h4 className="mb-2 font-bold">{bot.name}</h4>

            <PowerIndicator value={bot.power} />
          </div>

          {exceedsQuota && (
            <div className="absolute flex items-center border border-bad border-opacity-50 rounded-md bg-bad text-white bg-opacity-60 inset-0">
              <p className="w-full text-center text-sm">
                Team quota exceeded for {bot.symbol}
              </p>
            </div>
          )}
        </div>

        <div className="p-3 text-sm border-t border-ui-dark">
          <BotStats perks={perks} bot={bot} />
        </div>

        <div className="p-3 text-sm border-t border-ui-dark">
          {perks.length > 0 ? (
            <>
              {perks.map((perk, index) => (
                <Fragment key={perk.id}>
                  {index > 0 && ', '}
                  <InlinePerk perk={perk} />
                </Fragment>
              ))}
            </>
          ) : (
            <p className="text-ui-lighter">No perks applied.</p>
          )}
        </div>

        <div className="p-3 border-t border-ui-dark text-sm">
          <button
            className="text-ui-lightest hover:text-white underline"
            onClick={() => onRemove()}
          >
            Remove
          </button>
        </div>
      </div>
    </article>
  );
};
