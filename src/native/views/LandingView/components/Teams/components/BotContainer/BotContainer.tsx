import { FC } from 'react';
import { getOverflowingBots } from '../../../../../../../simulation/generator/utils';
import { BotDefinition, TeamDefinition } from '../../../../../../../types';
import BotSummary from '../BotSummary';

type Props = {
  readonly team: TeamDefinition;
  readonly onRemoveBot: (bot: BotDefinition) => void;
};

export const BotContainer: FC<Props> = ({ team, onRemoveBot }) => {
  const overflowing = getOverflowingBots(team.bots);

  if (team.bots.length === 0) {
    return (
      <div className="w-full absolute left-0 top-1/2 transform -translate-y-1/2">
        <p className="text-sm max-w-xs mx-auto text-ui-lightest text-center">
          No bots added yet &ndash; use the company search to create them!
        </p>
      </div>
    );
  }

  return (
    <div className="grid gap-3 grid-cols-2">
      {team.bots.map((bot) => (
        <BotSummary
          key={bot.id}
          team={team}
          bot={bot}
          onRemove={() => onRemoveBot(bot)}
          exceedsQuota={overflowing.includes(bot)}
        />
      ))}
    </div>
  );
};
