import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import { FC } from 'react';
import { Perk } from '../../../../../../../../../simulation/perks';
import Tooltip from '../../../../../../../../components/Tooltip';

type Props = {
  readonly perk: Perk;
  readonly disabled: boolean;
  readonly selected: boolean;
  readonly onClick: () => void;
};

const theme = (selected: boolean, disabled: boolean) =>
  classNames('flex w-full items-center rounded-md p-3', {
    'bg-ui-lighter hover:bg-ui-lightest': !(disabled || selected),
    'bg-white text-ui-darkest': selected,
    'cursor-not-allowed bg-ui-darker text-ui-light': disabled && !selected,
  });

export const BotPerk: FC<Props> = ({ perk, selected, disabled, onClick }) => {
  return (
    <Tooltip
      position="above"
      tip={
        <div>
          <p className="mb-3">{perk.metadata.description(perk)}</p>
          <p className="text-xs opacity-50">
            <span className="block font-bold">Reason</span>
            {perk.metadata.reason}
          </p>
        </div>
      }
    >
      <button onClick={() => onClick()} className={theme(selected, disabled)}>
        <FontAwesomeIcon icon={perk.metadata.icon} />{' '}
        <span className="text-sm ml-2 font-bold">{perk.metadata.name}</span>
      </button>
    </Tooltip>
  );
};
