import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC, useState } from 'react';
import { BOT_MAX_PERKS, TEAM_MAX_QUOTA } from '../../../../../../../constants';
import { getTeamPower } from '../../../../../../../simulation/generator/utils';
import { getPerks, Perk } from '../../../../../../../simulation/perks';
import { BotDefinition, TeamDefinition } from '../../../../../../../types';
import BotThumbnail from '../../../../../../components/BotThumbnail';
import Overlay from '../../../../../../components/Overlay';
import PerksModal from '../../../../../../components/PerksModal';
import PowerIndicator from '../../../../../../components/PowerIndicator';
import BotStats from '../../../../../../components/BotStats';
import BotPerkSelector from './components/BotPerkSelector';

type Props = {
  readonly template: BotDefinition;
  readonly team: TeamDefinition;
  readonly onAdd: (bot: BotDefinition) => void;
  readonly onCancel: () => void;
};

export const BotBuilder: FC<Props> = ({ team, template, onAdd, onCancel }) => {
  const [showPerksModal, setShowPerksModal] = useState(false);
  const [selectedPerks, setSelectedPerks] = useState<readonly Perk[]>([]);

  const getCannotSubmitReason = (): string | null => {
    if (
      team.bots
        .map((bot) => bot.symbol.toLowerCase())
        .includes(template.symbol.toLowerCase())
    ) {
      return 'Team already includes this company.';
    }

    const remainingQuota = TEAM_MAX_QUOTA - getTeamPower(team);
    const powerOverage = remainingQuota - template.power;

    if (powerOverage < 0) {
      return `This company would push the team over the maximum power threshold by ${Math.abs(
        powerOverage,
      ).toFixed(1)}.`;
    }

    return null;
  };

  const reason = getCannotSubmitReason();
  const perks = getPerks(template.availablePerks);

  return (
    <>
      <Overlay>
        <div className="flex items-center h-screen overflow-hidden p-5">
          <section className="flex flex-col bg-ui w-full max-w-lg max-h-full mx-auto shadow-lg rounded-md">
            <div className="flex-shrink-0 p-5 flex items-center">
              <div>
                <BotThumbnail team={team} bot={template} size={90} />
              </div>
              <div className="flex-grow pl-5">
                <p className="text-ui-lightest text-sm">{template.symbol}</p>
                <h4 className="text-lg font-bold">{template.name}</h4>

                <div className="pt-2">
                  <PowerIndicator value={template.power} />
                </div>
              </div>
            </div>

            <div className="flex-grow overflow-y-auto border-t border-ui-lighter">
              <div className="p-5">
                <h5 className="font-bold text-white">Attributes</h5>

                <p className="text-xs text-ui-lighter mb-2">
                  Attributes automatically generated from underlying company.
                </p>

                <BotStats perks={selectedPerks} bot={template} />
              </div>

              <div className="border-t border-ui-dark p-5">
                <div className="grid grid-cols-2">
                  <h5 className="font-bold text-white">
                    Perks ({selectedPerks.length}/{BOT_MAX_PERKS} selected)
                  </h5>
                  <div className="text-right">
                    <button
                      className="text-sm underline text-ui-lightest hover:text-white"
                      onClick={() => setShowPerksModal(true)}
                    >
                      View All Perks
                    </button>
                  </div>
                </div>

                <p className="text-xs text-ui-lighter mb-2">
                  Pick up to {BOT_MAX_PERKS} of the following perks available to
                  this bot:
                </p>

                <BotPerkSelector
                  limit={BOT_MAX_PERKS}
                  available={perks}
                  selected={selectedPerks}
                  onChange={(perks) => setSelectedPerks(perks)}
                />
              </div>
            </div>

            <div className="flex-shrink-0 p-5 grid gap-5 grid-cols-2 border-t border-ui-lighter">
              {reason && <div className="col-span-2 text-bad">{reason}</div>}
              {!reason &&
                selectedPerks.length <
                  Math.min(BOT_MAX_PERKS, perks.length) && (
                  <div className="col-span-2 text-yellow-200">
                    <FontAwesomeIcon icon={faExclamationTriangle} /> You can
                    still select additional perks.
                  </div>
                )}
              <button
                disabled={reason !== null}
                onClick={() =>
                  onAdd({
                    ...template,
                    appliedPerks: selectedPerks.map((perk) => perk.id),
                  })
                }
                className="w-full px-5 py-2 font-bold text-ui-dark bg-primary rounded-md disabled:bg-ui-dark disabled:cursor-not-allowed disabled:text-ui-light"
              >
                Confirm
              </button>
              <button
                onClick={() => onCancel()}
                className="w-full px-5 py-2 font-bold text-white bg-ui-darker hover:bg-ui-darkest rounded-md"
              >
                Cancel
              </button>
            </div>
          </section>
        </div>
      </Overlay>
      {showPerksModal && (
        <PerksModal
          available={perks}
          onClose={() => setShowPerksModal(false)}
        />
      )}
    </>
  );
};
