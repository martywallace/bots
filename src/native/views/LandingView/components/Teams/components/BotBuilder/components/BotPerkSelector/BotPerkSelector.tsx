import { FC } from 'react';
import { Perk } from '../../../../../../../../../simulation/perks';
import BotPerk from '../BotPerk';

type Props = {
  readonly limit: number;
  readonly selected: readonly Perk[];
  readonly available: readonly Perk[];
  readonly onChange: (perks: readonly Perk[]) => void;
};

export const BotPerkSelector: FC<Props> = ({
  limit,
  selected,
  available,
  onChange,
}) => {
  const hasPerk = (perk: Perk) => selected.some((test) => test.id === perk.id);

  const handleSelectPerk = (perk: Perk) => {
    onChange(
      hasPerk(perk)
        ? selected.filter((entry) => entry !== perk)
        : selected.concat(perk).slice(0, limit),
    );
  };

  return (
    <div className="grid gap-2 grid-cols-2 bg-ui-dark p-5 rounded-md">
      {available.length === 0 && (
        <p className="text-ui-lighter">No perks available.</p>
      )}
      {available.map((perk) => (
        <BotPerk
          key={perk.id}
          perk={perk}
          disabled={selected.length >= limit}
          selected={hasPerk(perk)}
          onClick={() => handleSelectPerk(perk)}
        />
      ))}
    </div>
  );
};
