import { FC } from 'react';
import PowerIndicator from '../../../../../../components/PowerIndicator';
import {
  getOverflowingBots,
  getTeamPower,
} from '../../../../../../../simulation/generator/utils';
import { TeamDefinition } from '../../../../../../../types';
import { TEAM_MAX_QUOTA } from '../../../../../../../constants';

type Props = {
  readonly definition: TeamDefinition;
  readonly onView: () => void;
  readonly onRemove: () => void;
};

export const TeamBlock: FC<Props> = ({ definition, onView, onRemove }) => {
  const overflowingBots = getOverflowingBots(definition.bots);

  return (
    <article className="relative flex flex-col bg-ui border border-ui-lighter rounded-sm shadow-lg h-full">
      <div className="items-center p-3">
        <span
          className="absolute rounded-md shadow-lg top-0 left-0 -ml-2 -mt-2 inline-block w-5 h-5 border border-white"
          style={{
            backgroundColor:
              '#' + definition.color.toString(16).padStart(6, '0'),
          }}
        />

        <h3 className="font-bold">{definition.name}</h3>
        <p className="text-ui-lightest text-sm">
          {getTeamPower(definition).toFixed(1)} / {TEAM_MAX_QUOTA.toFixed(1)}{' '}
          total power
          <div className="mt-1 rounded-md bg-ui-darkest">
            <div
              className="rounded-md shadow-md h-2 bg-white"
              style={{
                width: (getTeamPower(definition) / TEAM_MAX_QUOTA) * 100 + '%',
              }}
            />
          </div>
        </p>
      </div>

      <div className="border-t border-ui-light flex-grow">
        {definition.bots.map((bot) => (
          <div
            key={bot.id}
            className="grid gap-2 grid-cols-3 border-b border-ui-dark px-3 py-2 last:border-0"
          >
            <div className="flex items-center col-span-2">
              {overflowingBots.includes(bot) ? (
                <s>{bot.name}</s>
              ) : (
                <>{bot.name}</>
              )}
            </div>
            <div className="text-right text-ui-lighter">
              <PowerIndicator value={bot.power} short />
            </div>
          </div>
        ))}
      </div>

      <div className="p-3 border-t border-ui-light text-sm">
        <button
          className="inline-block mr-2 text-ui-lightest hover:text-white underline"
          onClick={() => onView()}
        >
          View
        </button>
        <button
          className="inline-block mr-2 text-ui-lightest hover:text-white underline"
          onClick={() => onRemove()}
        >
          Remove
        </button>
      </div>
    </article>
  );
};
