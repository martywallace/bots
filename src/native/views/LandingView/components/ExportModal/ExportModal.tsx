import { FC, useMemo, useState } from 'react';
import { LS_TEAMS } from '../../../../../constants';
import { Random } from '../../../../../simulation/engine/utils/random';
import { EncodedTeamDefinition, TeamDefinition } from '../../../../../types';
import Modal from '../../../../components/Modal';

type Props = {
  readonly team: TeamDefinition;
  readonly onClose: () => void;
};

export const ExportModal: FC<Props> = ({ onClose, team }) => {
  const code = useMemo<string | undefined>(() => {
    try {
      return btoa(JSON.stringify({ version: LS_TEAMS, data: team }));
    } catch (error: any) {
      console.warn(`Could not encode team: ${error.message}`);
    }

    return undefined;
  }, [team]);

  return (
    <Modal
      size="md"
      title="Export Team"
      actions={
        <button
          onClick={() => onClose()}
          className="block w-full mx-auto max-w-sm px-5 py-2 font-bold text-white bg-ui-darker hover:bg-ui-darkest rounded-md"
        >
          OK
        </button>
      }
    >
      <div className="p-5 w-full">
        <textarea
          className="font-mono text-xs bg-ui-darker shadow-sm border border-ui-light rounded focus:border-ui-lighter placeholder:text-ui focus:outline-none px-4 py-3 w-full"
          value={code}
          readOnly
          rows={16}
        />
      </div>
    </Modal>
  );
};
