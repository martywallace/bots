import { FC, Fragment, useMemo, useState } from 'react';
import { LS_TEAMS } from '../../../../../constants';
import { Random } from '../../../../../simulation/engine/utils/random';
import { getTeamPower } from '../../../../../simulation/generator/utils';
import { EncodedTeamDefinition, TeamDefinition } from '../../../../../types';
import Modal from '../../../../components/Modal';
import PowerIndicator from '../../../../components/PowerIndicator';

type Props = {
  readonly onImport: (team: TeamDefinition) => void;
  readonly onClose: () => void;
};

export const ImportModal: FC<Props> = ({ onClose, onImport }) => {
  const [code, setCode] = useState('');
  const [error, setError] = useState('');

  const team = useMemo<TeamDefinition | null>(() => {
    if (code.length > 0) {
      setError('');

      try {
        const data: EncodedTeamDefinition = JSON.parse(atob(code));

        if (data.version !== LS_TEAMS) {
          setError(
            'The provided import code is outdated and not compatible with the current version of the game.',
          );
          return null;
        }

        // Produce new IDs and colour for entities to avoid collisions.
        return {
          ...data.data,
          id: Random.id(),
          color: Math.round(Random.get() * 0xffffff),
          bots: data.data?.bots?.map((bot) => ({
            ...bot,
            id: Random.id(),
          })),
        };
      } catch (error: any) {
        setError(`Failed to parse import code: ${error.message}`);
      }
    }

    return null;
  }, [code]);

  return (
    <Modal
      size="md"
      title="Import Team"
      actions={
        <div className="grid mx-auto max-w-2xl gap-5 grid-cols-2">
          <button
            disabled={!team}
            onClick={() => team && onImport(team)}
            className="w-full px-5 py-2 font-bold text-ui-dark bg-primary rounded-md disabled:bg-ui-dark disabled:cursor-not-allowed disabled:text-ui-light"
          >
            Import
          </button>
          <button
            onClick={() => onClose()}
            className="w-full px-5 py-2 font-bold text-white bg-ui-darker hover:bg-ui-darkest rounded-md"
          >
            Cancel
          </button>
        </div>
      }
    >
      <div className="grid p-5 w-full gap-5 grid-cols-2">
        <div>
          <textarea
            className="font-mono text-xs bg-ui-darker shadow-sm border border-ui-light rounded focus:border-ui-lighter placeholder:text-ui focus:outline-none px-4 py-3 w-full"
            value={code}
            onChange={(event) => setCode(event.target.value)}
            placeholder="Paste import code here."
            rows={10}
          />
        </div>
        <div>
          {error ? (
            <p className="text-yellow-200 text-sm">{error}</p>
          ) : (
            <>
              {team ? (
                <>
                  <div className="mb-2">
                    <PowerIndicator value={getTeamPower(team)} />
                  </div>
                  <div className="mb-2">
                    <strong className="text-ui-lighter text-xs">Name</strong>
                    <br />
                    {team.name}
                  </div>
                  <div>
                    <strong className="text-ui-lighter text-xs">
                      Bots ({team.bots.length})
                    </strong>
                    <br />
                    {team.bots.map((bot, index) => (
                      <Fragment key={bot.id}>
                        {index > 0 && ', '}
                        {bot.symbol}
                      </Fragment>
                    ))}
                  </div>
                </>
              ) : (
                <p>Paste import code to import a team.</p>
              )}
            </>
          )}
        </div>
      </div>
    </Modal>
  );
};
