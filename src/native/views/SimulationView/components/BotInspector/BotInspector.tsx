import classNames from 'classnames';
import { Point } from 'pixi.js';
import { FC, Fragment } from 'react';
import { Bot } from '../../../../../simulation/engine/world/bot';
import { getPerks } from '../../../../../simulation/perks';
import InlinePerk from '../../../../components/InlinePerk';
import PowerIndicator from '../../../../components/PowerIndicator';

type Props = {
  readonly bot: Bot;
};

const theme = (onLeft: boolean) =>
  classNames(
    'fixed bg-ui-light text-white w-full max-w-sm rounded-md shadow-xl border border-ui-lighter bottom-0 z-20 m-20',
    {
      'left-0': onLeft,
      'right-0': !onLeft,
    },
  );

export const BotInspector: FC<Props> = ({ bot }) => {
  const screenPosition = bot.graphics.toGlobal(new Point());
  const onLeft = screenPosition.x > window.innerWidth / 2;

  return (
    <div className={theme(onLeft)}>
      <div className="p-5 border-b border-ui-lighter font-black">
        <span
          className="absolute rounded-md shadow-lg top-0 left-0 -ml-2 -mt-2 inline-block w-5 h-5 border border-white"
          style={{
            backgroundColor:
              '#' + bot.team.definition.color.toString(16).padStart(6, '0'),
          }}
        />{' '}
        {bot.team.definition.name}
      </div>
      <div className="flex p-5">
        <div className="flex-grow">
          <p className="opacity-70 text-xs">{bot.definition.symbol}</p>
          <h4 className="font-bold leading-snug">{bot.definition.name}</h4>

          <div className="text-sm pt-3">
            {bot.definition.appliedPerks.length > 0 ? (
              <>
                {getPerks(bot.definition.appliedPerks).map((perk, index) => (
                  <Fragment key={perk.id}>
                    {index > 0 && ', '}
                    <InlinePerk perk={perk} />
                  </Fragment>
                ))}
              </>
            ) : (
              <p className="text-ui-lighter">No perks applied.</p>
            )}
          </div>
        </div>

        <div className="flex-shrink-0">
          <PowerIndicator value={bot.definition.power} />
        </div>
      </div>
      <div className="p-5 border-t border-ui-lighter">
        <p className="text-xs mb-2">
          {bot.health} / {bot.stats.durability} durability
        </p>
        <div className="bg-ui-darker rounded-md">
          <div
            className="bg-white border border-white bg-opacity-90 h-4 rounded-md"
            style={{ width: (bot.health / bot.stats.durability) * 100 + '%' }}
          />
        </div>
      </div>
    </div>
  );
};
