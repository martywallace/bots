import { FC } from 'react';
import { TeamDefinition } from '../../../../../types';
import Overlay from '../../../../components/Overlay';

type Props = {
  readonly team: TeamDefinition;
  readonly onConfirm: () => void;
};

export const VictoryModal: FC<Props> = ({ team, onConfirm }) => {
  return (
    <Overlay>
      <div className="flex items-end py-20 h-screen overflow-hidden">
        <section className="flex flex-col bg-ui w-full max-w-lg mx-auto shadow-lg rounded-md p-5">
          <h4 className="text-lg mb-5 font-bold">Victory!</h4>
          <p className="mb-10">
            <strong>{team.name}</strong> was the winner.
          </p>

          <button
            className="w-full px-5 py-2 font-bold text-white bg-ui-darker hover:bg-ui-dark rounded-md"
            onClick={() => onConfirm()}
          >
            OK
          </button>
        </section>
      </div>
    </Overlay>
  );
};
