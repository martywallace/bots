import useLocalStorage from '@rehooks/local-storage';
import { FC, useEffect, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { LS_SEED, LS_TEAMS } from '../../../constants';
import { Simulation } from '../../../simulation';
import { Bot } from '../../../simulation/engine/world/bot';
import { TeamDefinition } from '../../../types';
import BotInspector from './components/BotInspector';
import VictoryModal from './components/VictoryModal';

export const SimulationView: FC = () => {
  const navigate = useNavigate();

  const [seed] = useLocalStorage(LS_SEED, '');
  const [teams] = useLocalStorage<readonly TeamDefinition[]>(LS_TEAMS, []);
  const [bot, setBot] = useState<Bot | null>(null);

  const [victor, setVictor] = useState<TeamDefinition | null>(null);

  const sceneRef = useRef<HTMLDivElement>(null);
  const simulationRef = useRef<Simulation>();

  useEffect(() => {
    // Start new simulation.
    simulationRef.current = new Simulation({
      onVictory: (team: TeamDefinition) => setVictor(team),
      onInspect: (bot) => setBot(bot),
      onStopInspect: () => setBot(null),
    });

    const sceneContainer = sceneRef.current;

    if (sceneContainer) {
      sceneContainer.appendChild(simulationRef.current.pixi.view);
    }

    simulationRef.current.start(seed, teams);

    return () => {
      simulationRef.current?.terminate();
    };
  }, [teams, seed]);

  return (
    <>
      <div className="relative w-screen h-screen bg-ui-darker">
        <div ref={sceneRef} className="w-full h-full" />
        <div className="absolute bottom-0 left-0 p-5">
          <Link to="/">Back</Link>
        </div>
      </div>

      {victor && <VictoryModal team={victor} onConfirm={() => navigate('/')} />}
      {bot && <BotInspector bot={bot} />}
    </>
  );
};
