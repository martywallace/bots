export const LS_TEAMS = 'teams-v8';
export const LS_SEED = 'seed-v2';

export const TEAM_MAX_QUOTA = 40;
export const BOT_MAX_PERKS = 3;
