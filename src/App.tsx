import { FC } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LandingView from './native/views/LandingView';
import SimulationView from './native/views/SimulationView';

export const App: FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingView />} />
        <Route path="/battle" element={<SimulationView />} />
      </Routes>
    </BrowserRouter>
  );
};
