import { Graphics, Point } from 'pixi.js';
import { Random } from '../../utils/random';

export class Star extends Graphics {
  public readonly velocity: Point;

  constructor() {
    super();

    this.beginFill(0xffffff, 0.1 + Random.get() * 0.5);
    this.drawCircle(0, 0, 0.05 + Random.get());
    this.endFill();

    const direction = 0.14;
    const speed = 0.01 + Random.get() * 0.8;

    this.velocity = new Point(
      0.01 + Math.cos(direction) * speed,
      0.01 + Math.sin(direction) * speed,
    );
  }
}
