import { Random } from '../../utils/random';
import { Star } from './star';

export function createStars(
  amount: number,
  containerWidth: number,
  containerHeight: number,
): readonly Star[] {
  const stars: Star[] = [];

  for (let i = 0; i < amount; i++) {
    const star = new Star();

    star.position.set(
      Random.get() * containerWidth,
      Random.get() * containerHeight,
    );

    stars.push(star);
  }

  return stars;
}
