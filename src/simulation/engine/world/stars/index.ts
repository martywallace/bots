import { Container } from 'pixi.js';
import { Simulation } from '../../..';
import { Random } from '../../utils/random';
import { Star } from './star';
import { createStars } from './utils';

export class Stars {
  private readonly stars: readonly Star[];

  constructor(container: Container, private readonly engine: Simulation) {
    this.stars = createStars(1000, engine.sceneWidth, engine.sceneHeight);
    this.stars.forEach((star) => container.addChild(star));
  }

  public tick(): void {
    for (const star of this.stars) {
      star.x += star.velocity.x;
      star.y += star.velocity.y;

      if (star.x >= this.engine.sceneWidth + 10) star.x = -10;
      if (star.y >= this.engine.sceneHeight + 10) star.y = -10;
    }
  }

  public shuffle(): void {
    for (const star of this.stars) {
      star.position.set(
        Random.between(-10, this.engine.sceneWidth + 10),
        Random.between(-10, this.engine.sceneHeight + 10),
      );
    }
  }
}
