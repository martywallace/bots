export enum WorldObjectTag {
  Projectile = 'projectile',
  Bot = 'bot',
}
