import { Point } from 'pixi.js';
import { Bot } from '../..';
import { Simulation } from '../../../../..';
import { Timer, TimerOptions } from '../../../../utils/timer';
import { MovementBehaviour } from '../interfaces';

export class BaseMovement implements MovementBehaviour {
  protected targetPoint: Point | null = null;

  protected readonly timer: Timer;

  constructor(
    protected readonly simulation: Simulation,
    options: TimerOptions,
  ) {
    this.timer = simulation.createTimer(options);
  }

  public attach(owner: Bot): void {
    this.timer.trigger();
    this.timer.start();
  }

  public detach(owner: Bot): void {
    this.simulation.destroyTimer(this.timer);
  }

  public tick(owner: Bot): Point {
    if (this.targetPoint) {
      const velocity = this.getVelocity(
        owner.position,
        this.targetPoint,
        owner.stats.movementSpeed,
      );

      return new Point(
        owner.position.x + (velocity.x ?? 0),
        owner.position.y + (velocity.y ?? 0),
      );
    }

    return owner.position;
  }

  protected getVelocity(
    location: Point,
    destination: Point,
    speed: number,
  ): Point {
    const distance = Math.sqrt(
      (destination.x - location.x) ** 2 + (destination.y - location.y) ** 2,
    );

    return new Point(
      distance
        ? ((destination.x - location.x) / distance) * Math.min(speed, distance)
        : 0,
      distance
        ? ((destination.y - location.y) / distance) * Math.min(speed, distance)
        : 0,
    );
  }
}
