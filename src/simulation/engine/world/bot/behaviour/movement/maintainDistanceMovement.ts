import { Point } from 'pixi.js';
import { Bot } from '../..';
import { Simulation } from '../../../../..';
import { Random } from '../../../../utils/random';
import { MovementBehaviour } from '../interfaces';
import { BaseMovement } from './baseMovement';

type MaintainDistanceMovementOptions = {
  readonly targetDistance: number;
  readonly maxOffsetAngle: number;
  readonly minTargetChangeFrequencyMilliseconds: number;
  readonly maxTargetChangeFrequencyMilliseconds: number;
  readonly maxHorizontalOffset: number;
  readonly maxVerticalOffset: number;
};

export class MaintainDistanceMovement
  extends BaseMovement
  implements MovementBehaviour
{
  constructor(
    simulation: Simulation,
    private readonly options: MaintainDistanceMovementOptions,
    owner: Bot,
  ) {
    super(simulation, {
      name: 'change_target_point',
      minimumDelayMs: options.minTargetChangeFrequencyMilliseconds,
      maximumDelayMs: options.maxTargetChangeFrequencyMilliseconds,
      action: () => this.changeTargetPoint(owner),
    });
  }

  public tick(owner: Bot): Point {
    if (this.targetPoint) {
      const velocity = this.getVelocity(
        owner.position,
        this.targetPoint,
        owner.stats.movementSpeed,
      );

      return new Point(
        owner.position.x + velocity.x,
        owner.position.y + velocity.y,
      );
    }

    return owner.position;
  }

  private changeTargetPoint(owner: Bot): void {
    this.targetPoint = this.selectTargetPoint(owner);
  }

  private selectTargetPoint(owner: Bot): Point | null {
    if (owner.target === null) {
      return null;
    }

    const angle =
      owner.angleTo(owner.target) +
      Random.between(-this.options.maxOffsetAngle, this.options.maxOffsetAngle);
    const distance = owner.distanceTo(owner.target);
    const buffer = 80;

    return new Point(
      Math.max(
        buffer,
        Math.min(
          this.options.maxHorizontalOffset - buffer,
          owner.position.x +
            Math.cos(angle) * (distance - this.options.targetDistance),
        ),
      ),
      Math.max(
        buffer,
        Math.min(
          this.options.maxVerticalOffset - buffer,
          owner.position.y +
            Math.sin(angle) * (distance - this.options.targetDistance),
        ),
      ),
    );
  }
}
