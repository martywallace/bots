import { Point } from 'pixi.js';
import { Bot } from '../..';
import { Simulation } from '../../../../..';
import { Random } from '../../../../utils/random';
import { MovementBehaviour } from '../interfaces';
import { BaseMovement } from './baseMovement';

type HoverAroundHomePointMovementOptions = {
  readonly originPoint: Point;
  readonly maxDistance: number;
  readonly minTargetChangeFrequencyMilliseconds: number;
  readonly maxTargetChangeFrequencyMilliseconds: number;
};

export class HoverAroundHomePointMovement
  extends BaseMovement
  implements MovementBehaviour
{
  constructor(
    simulation: Simulation,
    private readonly options: HoverAroundHomePointMovementOptions,
  ) {
    super(simulation, {
      name: 'change_target_point',
      minimumDelayMs: options.minTargetChangeFrequencyMilliseconds,
      maximumDelayMs: options.maxTargetChangeFrequencyMilliseconds,
      action: () => this.changeTargetPoint(),
    });
  }

  public tick(owner: Bot): Point {
    if (this.targetPoint) {
      const velocity = this.getVelocity(
        owner.position,
        this.targetPoint,
        owner.stats.movementSpeed,
      );

      return new Point(
        owner.position.x + velocity.x,
        owner.position.y + velocity.y,
      );
    }

    return owner.position;
  }

  private changeTargetPoint(): void {
    this.targetPoint = this.selectTargetPoint();
  }

  private selectTargetPoint(): Point {
    const angle = Random.get() * Math.PI * 2;
    const distance = Random.get() * this.options.maxDistance;

    return new Point(
      this.options.originPoint.x + Math.cos(angle) * distance,
      this.options.originPoint.y + Math.sin(angle) * distance,
    );
  }
}
