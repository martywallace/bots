import { Point } from 'pixi.js';
import { Bot } from '..';

export interface Behaviour {
  attach: (bot: Bot) => void;
  detach: (bot: Bot) => void;
}

/**
 * Defines bot behaviour for movement around the arena.
 */
export interface MovementBehaviour extends Behaviour {
  tick: (owner: Bot) => Point;
}

/**
 * Define behaviour for target selection.
 */
export interface TargetingBehaviour extends Behaviour {
  select: (owner: Bot, enemies: readonly Bot[]) => Bot | null;
}
