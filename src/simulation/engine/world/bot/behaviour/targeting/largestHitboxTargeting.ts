import { Bot } from '../..';
import { TargetingBehaviour } from '../interfaces';

export class LargestHitboxTargeting implements TargetingBehaviour {
  public attach(owner: Bot): void {
    //
  }

  public detach(owner: Bot): void {
    //
  }

  public select(owner: Bot, enemies: readonly Bot[]): Bot | null {
    let target: Bot | null = null;

    for (const enemy of enemies) {
      if (
        target === null ||
        // what matters for maximizing hit chance is the angular size of the target rather than its physical size
        this.calculateAngularSize(
          owner.distanceTo(enemy),
          enemy.stats.hitboxRadius,
        ) >
          this.calculateAngularSize(
            owner.distanceTo(target),
            target.stats.hitboxRadius,
          )
      ) {
        target = enemy;
      }
    }

    return target;
  }

  private calculateAngularSize(distance: number, hitboxRadius: number): number {
    return 2 * Math.tan(hitboxRadius / distance);
  }
}
