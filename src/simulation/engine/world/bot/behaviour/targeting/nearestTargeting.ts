import { Bot } from '../..';
import { TargetingBehaviour } from '../interfaces';

export class NearestTargeting implements TargetingBehaviour {
  public attach(owner: Bot): void {
    //
  }

  public detach(owner: Bot): void {
    //
  }

  public select(owner: Bot, enemies: readonly Bot[]): Bot | null {
    let target: Bot | null = null;

    for (const enemy of enemies) {
      if (
        target === null ||
        owner.distanceTo(enemy) < owner.distanceTo(target)
      ) {
        target = enemy;
      }
    }

    return target;
  }
}
