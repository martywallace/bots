import { Point } from 'pixi.js';
import { TeamDefinition } from '../../../../types';

export type TeamOptions = {
  readonly homePoint: Point;
};

export class Team {
  constructor(
    public readonly definition: TeamDefinition,
    public readonly options: TeamOptions,
  ) {}
}
