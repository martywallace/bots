import { World } from '..';
import { Simulation } from '../../..';
import { BotDefinition, BotStats } from '../../../../types';
import {
  applyPerkStatAdjustments,
  getPerk,
  getPerks,
  Perk,
  PerkID,
} from '../../../perks';
import { ExplosiveEndPerkSettings } from '../../../perks/explosiveEndPerk';
import { RegenerationPerkSettings } from '../../../perks/regenerationPerk';
import { SplitShotPerkSettings } from '../../../perks/splitShot';
import { BotGraphics } from '../../graphics/botGraphics';
import { Healthbar } from '../../ui/healthbar';
import { Random } from '../../utils/random';
import { Timer } from '../../utils/timer';
import { Bullet } from '../bullet';
import { WorldObjectTag } from '../enums';
import { WorldObject } from '../object';
import { Particles } from '../particles';
import { MovementBehaviour, TargetingBehaviour } from './behaviour/interfaces';
import { HoverAroundHomePointMovement } from './behaviour/movement/hoverAroundHomePointMovement';
import { MaintainDistanceMovement } from './behaviour/movement/maintainDistanceMovement';
import { LargestHitboxTargeting } from './behaviour/targeting/largestHitboxTargeting';
import { LowestHealthTargeting } from './behaviour/targeting/lowestHealthTargeting';
import { NearestTargeting } from './behaviour/targeting/nearestTargeting';
import { Team } from './team';

export class Bot extends WorldObject {
  public readonly tags = [WorldObjectTag.Bot];
  public readonly stats: BotStats;

  private fireTimer: Timer;
  private reloadTimer: Timer;
  private retargetTimer: Timer;
  private regenerationTimer: Timer | null = null;

  private _target: Bot | null = null;

  private ammo: number = Number.MAX_SAFE_INTEGER;
  private _health = Number.MAX_SAFE_INTEGER;

  private movementBehaviour: MovementBehaviour;
  private targetingBehaviour: TargetingBehaviour;

  private readonly healthbar: Healthbar;

  constructor(
    public readonly definition: BotDefinition,
    public readonly team: Team,
    public readonly simulation: Simulation,
  ) {
    super(new BotGraphics(team.definition.color, definition));

    // Compile stats from base definition stats + any relevant perks selected.
    this.stats = applyPerkStatAdjustments(
      definition.stats,
      getPerks(definition.appliedPerks),
    );

    this.ammo = this.stats.clipSize;
    this.health = this.stats.durability;

    this.healthbar = new Healthbar(this);

    this.movementBehaviour = new HoverAroundHomePointMovement(simulation, {
      originPoint: team.options.homePoint,
      minTargetChangeFrequencyMilliseconds: 500,
      maxTargetChangeFrequencyMilliseconds: 5000,
      maxDistance: 150,
    });

    // randomly select a targetting behaviour for now
    this.targetingBehaviour =
      Random.from([
        new NearestTargeting(),
        new LowestHealthTargeting(),
        new LargestHitboxTargeting(),
      ]) ?? new NearestTargeting();

    this.retargetTimer = simulation.createTimer({
      name: 'retarget',
      minimumDelayMs: 500,
      maximumDelayMs: 5000,
      action: () => this.changeTarget(),
    });

    this.reloadTimer = simulation.createTimer({
      name: 'reload',
      minimumDelayMs: this.stats.reloadCooldownMs,
      maximumDelayMs: this.stats.reloadCooldownMs,
      action: () => this.reload(),
    });

    this.fireTimer = simulation.createTimer({
      name: 'fire',
      minimumDelayMs: this.stats.fireCooldownMs,
      maximumDelayMs: this.stats.fireCooldownMs * 1.5,
      action: () => this.fire(),
    });
  }

  get health() {
    return this._health;
  }

  set health(newHealth) {
    this._health = newHealth;
  }

  public onAdded(world: World): void {
    this.retargetTimer.start();
    this.fireTimer.start();

    const regenerationPerk = this.getPerk<RegenerationPerkSettings>(
      PerkID.Regeneration,
    );

    if (regenerationPerk) {
      this.regenerationTimer = this.simulation.createTimer({
        name: 'regenerate',
        minimumDelayMs: regenerationPerk.settings.rate,
        maximumDelayMs: regenerationPerk.settings.rate,
        action: () =>
          (this._health = Math.min(
            this.stats.durability,
            Math.floor(
              this._health +
                this.stats.durability * regenerationPerk.settings.percentage,
            ),
          )),
      });

      this.regenerationTimer.start();
    }

    this.movementBehaviour.attach(this);

    world.uiGraphicsContainer.addChild(this.healthbar.graphics);

    this.graphics.on('mouseover', () => this.emit('mouseover'));
    this.graphics.on('mouseout', () => this.emit('mouseout'));

    super.onAdded(world);
  }

  public onRemoved(world: World): void {
    this.simulation.destroyTimer(this.retargetTimer);
    this.simulation.destroyTimer(this.fireTimer);
    this.simulation.destroyTimer(this.reloadTimer);

    this.regenerationTimer &&
      this.simulation.destroyTimer(this.regenerationTimer);

    this.movementBehaviour.detach(this);

    world.uiGraphicsContainer.removeChild(this.healthbar.graphics);
    this.graphics.removeAllListeners();

    this.emit('die', this);

    world.camera.shake(5, 20);

    world.add(
      new Particles({
        colors: [this.team.definition.color, 0xffffff],
        amount: Random.between(250, 400),
        minSpeed: 2,
        maxSpeed: 20,
      }),
      this.position,
    );

    const explosiveEndPerk = this.getPerk<ExplosiveEndPerkSettings>(
      PerkID.ExplosiveEnd,
    );

    if (explosiveEndPerk && this.world) {
      const explosionBulletCount = Random.between(
        explosiveEndPerk.settings.minBullets,
        explosiveEndPerk.settings.maxBullets,
      );

      for (let i = 0; i < explosionBulletCount; i++) {
        const angle = Random.between(0, 2 * Math.PI);
        this.world.add(new Bullet(this, angle, 10), this.position);
      }
    }

    super.onRemoved(world);
  }

  public tick(world: World): void {
    const newPosition = this.movementBehaviour.tick(this);

    this.position.set(newPosition.x, newPosition.y);

    if (this.graphics.alpha < 1) {
      this.graphics.alpha = Math.min(1, this.graphics.alpha + 0.05);
    }

    this.healthbar.percentage = this.health / this.stats.durability;
    this.healthbar.graphics.position.set(this.position.x, this.position.y - 24);

    if (this.target) {
      if (this.target.health <= 0) {
        this.changeTarget();
      }

      this.graphics.rotation = this.angleTo(this.target);
    }

    super.tick(world);
  }

  private reload(): void {
    this.ammo = this.stats.clipSize;

    this.fireTimer.start();
    this.reloadTimer.stop();
  }

  public fire(): void {
    if (!this.world) return;

    if (this._target) {
      const targetAngle = this.angleTo(this._target);

      for (let i = 0; i < Math.min(this.ammo, this.getBulletCount()); i++) {
        const fireAngle = Random.gaussianRandom(
          targetAngle,
          this.stats.accuracyAngle,
        );

        this.world.add(new Bullet(this, fireAngle, 10), this.position);
        this.ammo -= 1;
      }
    }

    if (this.ammo <= 0) {
      this.fireTimer.stop();
      this.reloadTimer.start();
    }
  }

  public takeDamage(amount: number): void {
    if (this.world) {
      if (this.world.engine.victor) {
        // Simulation already has a victorious team, skip additional damage.
        return;
      }

      this.graphics.alpha = 0.4;
      this.health = Math.max(0, this.health - amount);

      if (this.health <= 0) {
        this.world.remove(this);
      }
    }
  }

  private getBulletCount(): number {
    const perk = this.getPerk<SplitShotPerkSettings>(PerkID.SplitShot);

    if (perk) {
      if (Random.roll(perk.settings.chance)) return perk.settings.bullets;
    }

    return 1;
  }

  private changeTarget(): void {
    if (this.world) {
      const enemies = Array.from(
        this.world.findByTag<Bot>(WorldObjectTag.Bot),
      ).filter((bot) => bot.team !== this.team);

      this._target = this.targetingBehaviour.select(this, enemies);

      if (this._target) {
        this.setMovementBehaviour(
          new MaintainDistanceMovement(
            this.simulation,
            {
              maxOffsetAngle: Math.PI / 6,
              targetDistance: Random.between(200, 800),
              minTargetChangeFrequencyMilliseconds: 250,
              maxTargetChangeFrequencyMilliseconds: 1000,
              maxHorizontalOffset: this.world?.engine.sceneWidth ?? 1000,
              maxVerticalOffset: this.world?.engine.sceneHeight ?? 1000,
            },
            this,
          ),
        );
      } else {
        this.setMovementBehaviour(
          new HoverAroundHomePointMovement(this.simulation, {
            originPoint: this.team.options.homePoint,
            minTargetChangeFrequencyMilliseconds: 500,
            maxTargetChangeFrequencyMilliseconds: 5000,
            maxDistance: 150,
          }),
        );
      }
    }
  }

  private setMovementBehaviour(behaviour: MovementBehaviour): void {
    this.movementBehaviour.detach(this);
    this.movementBehaviour = behaviour;
    this.movementBehaviour.attach(this);
  }

  public getPerk<T>(id: PerkID): Perk<T> | null {
    if (this.definition.appliedPerks.includes(id)) {
      return getPerk(id) as Perk<T>;
    }

    return null;
  }

  public get target(): Bot | null {
    return this._target;
  }
}
