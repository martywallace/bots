import { Container, Point } from 'pixi.js';
import { Random } from '../utils/random';
import { Timer } from '../utils/timer';

export class Camera {
  private readonly position: Point;
  private readonly targetPosition: Point;

  private shakeIntensity: number = 5;
  private shakeTimer: number = 0;

  constructor(x: number, y: number) {
    this.position = new Point(x, y);
    this.targetPosition = this.position.clone();
  }

  public look(x: number, y: number, force: boolean = false) {
    this.targetPosition.set(x, y);

    if (force) this.position.set(x, y);
  }

  public shake(intensity: number, duration: number): void {
    this.shakeIntensity = intensity;
    this.shakeTimer = duration;
  }

  public adjust(
    graphics: Container,
    sceneWidth: number,
    sceneHeight: number,
  ): void {
    this.position.x -= (this.position.x - this.targetPosition.x) / 40;
    this.position.y -= (this.position.y - this.targetPosition.y) / 40;

    graphics.position.set(
      sceneWidth / 2 - this.position.x,
      sceneHeight / 2 - this.position.y,
    );

    if (this.shakeTimer > 0) {
      this.shakeTimer -= 1;

      graphics.position.x += Random.between(
        -this.shakeIntensity,
        this.shakeIntensity,
      );
      graphics.position.y += Random.between(
        -this.shakeIntensity,
        this.shakeIntensity,
      );

      this.shakeIntensity -= this.shakeIntensity / 20;
    }
  }
}
