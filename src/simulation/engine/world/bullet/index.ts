import { Point } from 'pixi.js';
import { World } from '..';
import { BulletGraphics } from '../../graphics/bulletGraphics';
import { WorldObject } from '../object';
import { Bot } from '../bot';
import { WorldObjectTag } from '../enums';
import { PerkID } from '../../../perks';
import { ReflectPerkSettings } from '../../../perks/reflectPerk';
import { Random } from '../../utils/random';
import { Particles } from '../particles';

export class Bullet extends WorldObject {
  private tainted: boolean = false;

  public readonly velocity: Point;
  public readonly tags = [WorldObjectTag.Projectile];

  constructor(
    private readonly owner: Bot,
    angle: number,
    private readonly speed: number,
  ) {
    super(new BulletGraphics(owner.team.definition.color));

    this.velocity = new Point(Math.cos(angle) * speed, Math.sin(angle) * speed);
    this.graphics.rotation = Math.atan2(this.velocity.y, this.velocity.x);
  }

  public onAdded(world: World): void {
    setTimeout(() => world.remove(this), 8000);

    super.onAdded(world);
  }

  public tick(world: World): void {
    this.position.x += this.velocity.x;
    this.position.y += this.velocity.y;

    // If the bullet is "tainted", it is able to collide with any bots, even
    // from the owning team.
    const enemies = !this.tainted
      ? Array.from(world.findByTag<Bot>(WorldObjectTag.Bot)).filter(
          (bot) => bot.team !== this.owner.team,
        )
      : Array.from(world.findByTag<Bot>(WorldObjectTag.Bot));

    for (const enemy of enemies) {
      if (this.distanceTo(enemy) < enemy.stats.hitboxRadius) {
        this.collide(enemy, world);
      }
    }

    super.tick(world);
  }

  private collide(bot: Bot, world: World): void {
    if (!this.tainted) {
      const reflectPerk = bot.getPerk<ReflectPerkSettings>(PerkID.Reflect);

      if (reflectPerk) {
        if (Random.roll(reflectPerk.settings.chance)) {
          this.tainted = true;

          const angle = -bot.angleTo(this);

          this.position.set(
            bot.position.x +
              Math.cos(angle) * bot.definition.stats.hitboxRadius,
            bot.position.y +
              Math.sin(angle) * bot.definition.stats.hitboxRadius,
          );

          this.velocity.set(
            Math.cos(angle) * this.speed,
            Math.sin(angle) * this.speed,
          );

          this.graphics.rotation = angle;

          return;
        }
      }
    }

    this.applyDamage(bot, world);
  }

  private applyDamage(bot: Bot, world: World): void {
    const oppositeAngle =
      Math.atan2(this.velocity.y, this.velocity.x) + Math.PI;

    world.add(
      new Particles({
        colors: [this.owner.team.definition.color, bot.team.definition.color],
        amount: Random.between(10, 30),
        startAngle: oppositeAngle - 0.4,
        endAngle: oppositeAngle + 0.4,
      }),
      this.position,
    );

    world.remove(this);

    bot.takeDamage(this.owner.stats.damage);
  }
}
