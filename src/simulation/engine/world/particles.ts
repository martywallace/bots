import { Container, Graphics } from 'pixi.js';
import { World } from '.';
import { Random } from '../utils/random';
import { WorldObject } from './object';

export type ParticlesOptions = {
  readonly amount: number;
  readonly colors: readonly number[];
  readonly minSpeed?: number;
  readonly maxSpeed?: number;
  readonly startAngle?: number;
  readonly endAngle?: number;
};

class Particle extends Graphics {
  constructor(
    private readonly owner: Particles,
    public readonly speed: number,
  ) {
    super();

    const size = Random.between(0.5, 8);

    this.beginFill(Random.from(owner.options.colors) ?? 0xffffff);
    this.drawRect(-size / 2, -1, size, 2);
    this.endFill();

    this.alpha = Random.between(0.5, 1);

    this.rotation = Random.between(
      owner.options.startAngle ?? 0,
      owner.options.endAngle ?? Math.PI * 2,
    );
  }
}

export class Particles extends WorldObject {
  public readonly tags = [];

  private particles: Set<Particle>;

  constructor(public readonly options: ParticlesOptions) {
    super(new Container());

    this.particles = new Set();
  }

  public onAdded(world: World): void {
    for (let i = 0; i < this.options.amount; i++) {
      const particle = (this.graphics as Container).addChild(
        new Particle(
          this,
          Random.between(
            this.options.minSpeed ?? 1,
            this.options.maxSpeed ?? 5,
          ),
        ),
      );

      this.particles.add(particle);
    }
  }

  public onRemoved(world: World): void {
    //
  }

  public tick(world: World): void {
    for (const particle of this.particles) {
      particle.x += Math.cos(particle.rotation) * particle.speed;
      particle.y += Math.sin(particle.rotation) * particle.speed;

      particle.alpha -= 0.05;

      if (particle.alpha <= 0) {
        this.particles.delete(particle);
        this.graphics.removeChild(particle);

        if (this.particles.size === 0) {
          world.remove(this);
        }
      }
    }
  }
}
