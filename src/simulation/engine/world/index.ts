import { Container, Point } from 'pixi.js';
import { Simulation } from '../..';
import { Bot } from './bot';
import { Camera } from './camera';
import { WorldObjectTag } from './enums';
import { WorldObject } from './object';
import { Stars } from './stars';

export class World {
  public readonly graphics: Container;
  public readonly objectsGraphicsContainer: Container;
  public readonly uiGraphicsContainer: Container;

  public readonly camera: Camera;

  private readonly beings: Set<WorldObject>;
  private readonly removed: Set<WorldObject>;

  private readonly tags: Map<WorldObjectTag, Set<WorldObject>>;
  private readonly stars: Stars;

  constructor(public readonly engine: Simulation) {
    this.camera = new Camera(engine.sceneWidth / 2, engine.sceneHeight / 2);

    this.beings = new Set();
    this.removed = new Set();

    this.tags = new Map();
    this.stars = new Stars(engine.stage, engine);

    this.graphics = engine.stage.addChild(new Container());
    this.objectsGraphicsContainer = this.graphics.addChild(new Container());
    this.uiGraphicsContainer = this.graphics.addChild(new Container());
  }

  public add<T extends WorldObject>(being: T, position: Point): T {
    being.position.set(position.x, position.y);

    this.objectsGraphicsContainer.addChild(being.graphics);
    this.beings.add(being);

    for (const tag of being.tags) {
      const list = this.tags.get(tag);

      if (!list) {
        this.tags.set(tag, new Set([being]));
      } else {
        list.add(being);
      }
    }

    being.onAdded(this);

    return being;
  }

  public remove(being: WorldObject): void {
    // Flag for removal.
    this.removed.add(being);
  }

  public tick(): void {
    // Update scenery.
    this.stars.tick();

    // Update all beings.
    for (const being of this.beings) {
      try {
        being.tick(this);
      } catch (error: any) {
        console.warn(`Unable to call tick() on entity: ${error.message}`);
      }

      being.graphics.position.set(being.position.x, being.position.y);
    }

    // Remove any tagged for removal in the previous update stage.
    this.finalizeRemoval(Array.from(this.removed));
    this.removed.clear();

    const middle = this.getBotCentrePoint();

    this.camera.look(middle.x, middle.y);

    // Update to reflect camera location.
    this.camera.adjust(
      this.graphics,
      this.engine.sceneWidth,
      this.engine.sceneHeight,
    );
  }

  private finalizeRemoval(beings: readonly WorldObject[]): void {
    for (const being of beings) {
      if (this.beings.has(being)) {
        this.objectsGraphicsContainer.removeChild(being.graphics);
        this.beings.delete(being);

        for (const tag of being.tags) {
          const list = this.tags.get(tag);

          if (list) {
            list.delete(being);
          }
        }

        being.onRemoved(this);
      }
    }
  }

  public findByTag<T extends WorldObject = WorldObject>(
    tag: WorldObjectTag,
  ): Set<T> {
    return this.tags.get(tag) as Set<T>;
  }

  public getBotCentrePoint(): Point {
    const positions = Array.from(this.findByTag<Bot>(WorldObjectTag.Bot)).map(
      (bot) => bot.position,
    );

    if (positions.length > 0) {
      const x = positions.map((point) => point.x);
      const y = positions.map((point) => point.y);

      const lx = Math.max(...x);
      const sx = Math.min(...x);
      const ly = Math.max(...y);
      const sy = Math.min(...y);

      // Calculate the center between the smallest and largest position across
      // each axis.
      return new Point(sx + (lx - sx) / 2, sy + (ly - sy) / 2);
    }

    return this.engine.getSceneCenter();
  }

  public handleResize(): void {
    this.stars.shuffle();
  }

  public destroy(): void {
    this.finalizeRemoval(Array.from(this.beings));
    this.objectsGraphicsContainer.destroy();
  }
}
