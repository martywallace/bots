import EventEmitter from 'events';
import { DisplayObject, Point } from 'pixi.js';
import { World } from '.';
import { WorldObjectTag } from './enums';

export abstract class WorldObject extends EventEmitter {
  public readonly position: Point;

  private _world: World | null = null;

  constructor(public readonly graphics: DisplayObject) {
    super();

    this.position = new Point();
  }

  public onAdded(world: World): void {
    this._world = world;
  }

  public onRemoved(world: World): void {
    this._world = null;
  }

  public tick(world: World): void {
    //
  }

  public distanceTo(being: WorldObject): number {
    const x = being.position.x - this.position.x;
    const y = being.position.y - this.position.y;

    return Math.sqrt(x * x + y * y);
  }

  public angleTo(being: WorldObject): number {
    const x = being.position.x - this.position.x;
    const y = being.position.y - this.position.y;

    return Math.atan2(y, x);
  }

  protected get world(): World | null {
    return this._world;
  }

  public abstract get tags(): readonly WorldObjectTag[];
}
