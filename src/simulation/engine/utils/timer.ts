import { Random } from './random';

export type TimerOptions = {
  readonly name: string;
  readonly minimumDelayMs: number;
  readonly maximumDelayMs: number;
  readonly logging?: boolean;
  readonly action: () => void;
};

export class Timer {
  private lastTriggered: number = 0;
  private lastStopped: number = 0;

  private timer: NodeJS.Timeout | null = null;

  constructor(private readonly options: TimerOptions) {}

  public start(): void {
    this.options.logging &&
      console.log(
        `start: ${this.options.name}: ${this.options.minimumDelayMs}`,
      );

    if (this.timer) {
      this.stop();
    }

    this.next();
  }

  public stop(): void {
    this.options.logging && console.log(`stop: ${this.options.name}`);

    if (this.timer) {
      clearTimeout(this.timer);
    }

    this.timer = null;
    this.lastStopped = Date.now();
  }

  public trigger(): void {
    try {
      this.options.logging && console.log(`action: ${this.options.name}`);

      this.options.action();
    } catch (error: any) {
      console.warn(`Failed timer action: ${error.message}`);
    }
  }

  private getDiff(): number {
    return this.lastTriggered && this.lastStopped
      ? Math.max(0, this.lastStopped - this.lastTriggered)
      : 0;
  }

  private next(): void {
    const delay = Math.ceil(
      Random.between(this.options.minimumDelayMs, this.options.maximumDelayMs),
    );

    this.timer = setTimeout(() => {
      this.trigger();
      this.timer && this.next();
    }, delay - this.getDiff());

    this.lastTriggered = Date.now();
  }

  public get running(): boolean {
    return !!this.timer;
  }
}
