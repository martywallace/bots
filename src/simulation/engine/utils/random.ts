import seedrandom from 'seedrandom';

export class Random {
  private static generator: () => number = Math.random;

  public static seed(seed: string): void {
    this.generator = seedrandom(seed);
  }

  public static get(): number {
    return this.generator();
  }

  public static between(min: number, max: number): number {
    return this.get() * (max - min) + min;
  }

  // normally distributed random variable, see https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
  public static gaussianRandom(mean: number, std: number): number {
    const uniformNormal =
      Math.sqrt(-2 * Math.log(1 - this.get())) *
      Math.cos(2 * Math.PI * this.get());
    return uniformNormal * std + mean;
  }

  public static roll(chance: number): boolean {
    return this.get() <= chance;
  }

  public static from<T>(list: readonly T[]): T | null {
    if (list.length === 0) return null;

    return list[Math.floor(this.get() * list.length)];
  }

  public static id(length: number = 16): string {
    return (
      [...Array(length)]
        // No need for seeding here.
        .map(() => Math.floor(Math.random() * 16).toString(16))
        .join('')
    );
  }
}
