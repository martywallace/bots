import { Graphics } from 'pixi.js';

export class BulletGraphics extends Graphics {
  constructor(color: number) {
    super();

    this.lineStyle(0.5, 0xffffff, 0.4);
    this.beginFill(color, 1);
    this.drawRect(-6, -2, 12, 4);
    this.endFill();
  }
}
