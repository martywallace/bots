import { Graphics, Rectangle } from 'pixi.js';
import { BotDefinition } from '../../../types';

export class BotGraphics extends Graphics {
  constructor(color: number, definition: BotDefinition) {
    super();

    const x = -1 * definition.stats.hitboxRadius;
    const y = -1 * definition.stats.hitboxRadius;
    const w = 2 * definition.stats.hitboxRadius;
    const h = 2 * definition.stats.hitboxRadius;

    this.lineStyle(1, 0xffffff, 0.5)
      .beginFill(color, 1)
      .drawRoundedRect(x, y, w, h, 4);

    this.interactive = true;
    this.hitArea = new Rectangle(x - 5, y - 5, w + 10, h + 10);
    this.cursor = 'zoom-in';

    this.endFill();
  }
}
