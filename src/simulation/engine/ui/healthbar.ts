import { Graphics, Text } from 'pixi.js';
import { Bot } from '../world/bot';

export class Healthbar {
  public readonly graphics: Graphics;

  private readonly background: Graphics;
  private readonly fill: Graphics;

  constructor(private readonly owner: Bot) {
    const width = 28;

    this.graphics = new Graphics();

    this.background = this.graphics.addChild(new Graphics());

    this.background.position.x = -width / 2;
    this.background.position.y = -this.owner.definition.stats.hitboxRadius;
    this.background
      .beginFill(0x323649)
      .drawRoundedRect(0, 0, width, 4, 4)
      .endFill();

    this.fill = this.graphics.addChild(new Graphics());

    this.fill.position.x = -width / 2;
    this.fill.position.y = -this.owner.definition.stats.hitboxRadius;
    this.fill
      .lineStyle(1, 0xffffff)
      .beginFill(0xffffff, 0.8)
      .drawRoundedRect(0, 0, width, 4, 4)
      .endFill();

    const text = this.graphics.addChild(
      new Text(owner.definition.symbol.toUpperCase(), {
        fill: 0xffffff,
        align: 'center',
        fontSize: 9,
      }),
    );

    text.alpha = 0.7;

    text.position.set(
      -text.width / 2,
      -owner.definition.stats.hitboxRadius - 12,
    );
  }

  public set percentage(value: number) {
    this.fill.scale.x = value;
  }
}
