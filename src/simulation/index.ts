import EventEmitter from 'events';
import { Application, Container, Point } from 'pixi.js';
import { TeamDefinition } from '../types';
import { Random } from './engine/utils/random';
import { Timer, TimerOptions } from './engine/utils/timer';
import { World } from './engine/world';
import { Bot } from './engine/world/bot';
import { Team } from './engine/world/bot/team';
import { WorldObjectTag } from './engine/world/enums';
import { getOverflowingBots } from './generator/utils';

export type SimulationOptions = {
  readonly onVictory: (team: TeamDefinition) => void;
  readonly onInspect: (bot: Bot) => void;
  readonly onStopInspect: () => void;
};

export class Simulation extends EventEmitter {
  public readonly pixi: Application;
  public readonly world: World;

  private readonly timers: Set<Timer>;
  private readonly pausedTimers: Set<Timer>;

  private _paused: boolean = false;
  private _victor: TeamDefinition | null = null;

  constructor(private readonly options: SimulationOptions) {
    super();

    this.pixi = new Application({
      backgroundColor: 0x424861,
      resizeTo: window,
      antialias: true,
    });

    this.timers = new Set();
    this.pausedTimers = new Set();

    this.world = new World(this);

    this.pixi.ticker.add(() => this.tick());
    this.pixi.renderer.addListener('resize', () => this.world.handleResize());
  }

  public createTimer(options: TimerOptions): Timer {
    const timer = new Timer(options);

    this.timers.add(timer);

    return timer;
  }

  public destroyTimer(timer: Timer): void {
    timer.stop();
    this.timers.delete(timer);
  }

  public start(seed: string, teams: readonly TeamDefinition[]): void {
    Random.seed(seed);

    // Define team starting positions evenly around a circular arena.
    const angleBetweenTeams = (2 * Math.PI) / teams.length;
    const angles = teams.map((_, i) => i * angleBetweenTeams);
    const buffer = this.sceneRadius * 0.25;

    for (const [index, teamDefinition] of teams.entries()) {
      const homePointAngle = angles[index];

      const team = new Team(teamDefinition, {
        homePoint: new Point(
          this.getSceneCenter().x +
            Math.cos(homePointAngle) * (this.sceneRadius - buffer),
          this.getSceneCenter().y +
            Math.sin(homePointAngle) * (this.sceneRadius - buffer),
        ),
      });

      const overflowingBots = getOverflowingBots(teamDefinition.bots);

      for (const botDefinition of teamDefinition.bots) {
        if (overflowingBots.includes(botDefinition)) {
          // Don't include bots that exceed quota.
          continue;
        }

        const startAngle = Random.get() * Math.PI * 2;
        const startDistance = Random.between(20, 100);

        const startPosition = new Point(
          team.options.homePoint.x + Math.cos(startAngle) * startDistance,
          team.options.homePoint.y + Math.sin(startAngle) * startDistance,
        );

        const bot = this.world.add(
          new Bot(botDefinition, team, this),
          startPosition,
        );

        bot.graphics.rotation = startAngle;

        bot
          .addListener('die', () => this.handleBotDie())
          .addListener('mouseover', () => this.handleInspect(bot))
          .addListener('mouseout', () => this.handleStopInspect());
      }
    }

    const middle = this.world.getBotCentrePoint();

    // Immediately adjust camera to the middle of the room.
    this.world.camera.look(middle.x, middle.y, true);
  }

  private handleInspect(bot: Bot): void {
    this.pause();
    this.options.onInspect(bot);
  }

  private handleStopInspect(): void {
    this.unpause();
    this.options.onStopInspect();
  }

  public terminate(): void {
    this.world.destroy();
    this.pixi.destroy(true);
  }

  private tick(): void {
    if (this._paused) return;

    this.world.tick();
  }

  private handleBotDie(): void {
    const bots = Array.from(this.world.findByTag<Bot>(WorldObjectTag.Bot));
    const teams = Array.from(new Set(bots.map((bot) => bot.team)));

    if (teams.length === 1) {
      this._victor = teams[0].definition;
      this.options.onVictory(this._victor);
    }
  }

  public getSceneCenter(): Point {
    return new Point(
      this.pixi.renderer.width / 2,
      this.pixi.renderer.height / 2,
    );
  }

  public pause(): void {
    if (!this._paused) {
      // Capture timers that are currently running so we can resume them.
      for (const timer of this.timers) {
        if (timer.running) {
          timer.stop();
          this.pausedTimers.add(timer);
        }
      }
    }

    this._paused = true;
  }

  public unpause(): void {
    if (this._paused) {
      // Resume any timers.
      for (const timer of this.pausedTimers) {
        timer.start();
      }

      this.pausedTimers.clear();
    }

    this._paused = false;
  }

  public get stage(): Container {
    return this.pixi.stage;
  }

  public get sceneWidth(): number {
    return this.pixi.renderer.width;
  }

  public get sceneHeight(): number {
    return this.pixi.renderer.height;
  }

  public get sceneRadius(): number {
    return Math.min(this.sceneWidth, this.sceneHeight) / 2;
  }

  public get victor(): TeamDefinition | null {
    return this._victor;
  }
}
