import { getCompany, Response } from '../../api/queries/getCompany';
import { BotType } from '../../enums';
import { BotDefinition } from '../../types';
import { Random } from '../engine/utils/random';
import { resolvePerks } from '../perks';

export function generateBot(company: Response['Company']): BotDefinition {
  const power = Math.log10(company.analysisValue.marketCapUSD);

  return {
    id: Random.id(),
    symbol: company.uniqueSymbol,
    name: company.name,
    type: BotType.Melee,
    power: power,
    stats: {
      durability: 1 + Math.ceil(30 * power * (1 + company.score.health / 3)),
      damage: 10 + Math.ceil(10 * power / 6),
      clipSize: 15 + Math.ceil(6 - company.score.future),
      fireCooldownMs: 150 + 150 * (1 - company.score.value / 6),
      reloadCooldownMs: 2000,
      accuracyAngle: Math.PI / 45 / (1 + company.score.value / 6),
      hitboxRadius: 10 * (1 + company.score.health / 6),
      movementSpeed: 1 + 0.2 * company.score.future,
    },
    availablePerks: resolvePerks(company).map((perk) => perk.id),
    appliedPerks: [],
  };
}

export async function loadBot(symbol: string): Promise<BotDefinition> {
  // Load the company data from GraphQL.
  const company = await getCompany(symbol);

  // Transform it into a bot definition.
  return generateBot(company);
}
