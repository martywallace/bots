import { TEAM_MAX_QUOTA } from '../../constants';
import { BotDefinition, TeamDefinition } from '../../types';

/**
 * Returns the bots that exceed the maximum team quota, starting from the
 * beginning of the input list and working through to the end, discarding any
 * along the way that would exceed the maximum.
 *
 * @param bots The input bots.
 */
export function getOverflowingBots(
  bots: readonly BotDefinition[],
): readonly BotDefinition[] {
  let remainingQuota = TEAM_MAX_QUOTA;

  const overflowing: BotDefinition[] = [];

  for (const bot of bots) {
    if (remainingQuota - bot.power < 0) {
      overflowing.push(bot);
      continue;
    }

    remainingQuota -= bot.power;
  }

  return overflowing;
}

/**
 * Get the combined power of the target team, skipping bots which do not fit
 * within the maximum team quota.
 *
 * @param team The target team.
 */
export function getTeamPower(team: TeamDefinition): number {
  const overflowing = getOverflowingBots(team.bots);

  return (
    team.bots
      // Exclude bots that can't be included in the team.
      .filter((bot) => !overflowing.includes(bot))
      .reduce((total, bot) => total + bot.power, 0)
  );
}

export function getAccuracyPercentage(angle: number): number {
  return 1 - angle / (Math.PI / 2);
}
