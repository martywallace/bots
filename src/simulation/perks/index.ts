import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Company } from '../../api/queries/getCompany';
import { BotStats } from '../../types';
import { AITargetingPerk } from './aiTargetingPerk';
import { BurstPerk } from './burstPerk';
import { DeepPocketsPerk } from './deepPocketsPerk';
import { ExplosiveEndPerk } from './explosiveEndPerk';
import { FortifiedPerk } from './fortifiedPerk';
import { LaserPerk } from './laserPerk';
import { ReflectPerk } from './reflectPerk';
import { RegenerationPerk } from './regenerationPerk';
import { SplitShotPerk } from './splitShot';

export enum PerkID {
  Fortified = 'FORTIFIED',
  SplitShot = 'SPLIT_SHOT',
  Reflect = 'REFLECT',
  Laser = 'LASER',
  AITargeting = 'AI_TARGETING',
  Regeneration = 'REGENERATION',
  DeepPockets = 'DEEP_POCKETS',
  ExplosiveEnd = 'EXPLOSIVE_END',
  Burst = 'BURST',
}

export type PerkMetadata = {
  readonly name: string;
  readonly description: (perk: Perk) => string;
  readonly reason: string;
  readonly icon: IconProp;
};

export type PerkStatAdjustmentType = 'percent' | 'absolute';

export type PerkStatAdjustments = Partial<{
  readonly [K in keyof BotStats]: {
    readonly type: PerkStatAdjustmentType;
    readonly value: BotStats[K];
  };
}>;

export type PerkStatAdjustment<K extends keyof BotStats> = {
  readonly stat: K;
  readonly type: PerkStatAdjustmentType;
  readonly value: BotStats[K];
};

export interface Perk<PerkSettings = any> {
  readonly id: PerkID;
  readonly metadata: PerkMetadata;
  readonly settings: PerkSettings;

  /**
   * Stat adjustments that this perk provides.
   */
  readonly stats: PerkStatAdjustments;

  /**
   * Whether or not this perk is valid for the input company.
   */
  readonly valid: (company: Company) => boolean;
}

const perks = new Map<PerkID, Perk>();

function register(perk: Perk): void {
  perks.set(perk.id, perk);
}

/**
 * Resolve perks that are available based on the input company.
 *
 * @param company The input company.
 */
export function resolvePerks(company: Company): readonly Perk[] {
  return Array.from(perks.values()).filter((perk) => perk.valid(company));
}

export function getPerk(id: PerkID): Perk | undefined {
  return perks.get(id);
}

export function getPerks(ids: readonly PerkID[]): readonly Perk[] {
  const result: Perk[] = [];

  for (const id of ids) {
    const perk = perks.get(id);

    if (perk) {
      result.push(perk);
    }
  }

  return result;
}

/**
 * Extracts stat adjustments from input perks and sorts them in a way that is
 * usable for sequential application.
 *
 * @param perks The input perks.
 */
export function preparePerkStatAdjustments(
  perks: readonly Perk[],
): readonly PerkStatAdjustment<keyof BotStats>[] {
  const result: PerkStatAdjustment<keyof BotStats>[] = [];

  for (const perk of perks) {
    for (const [stat, { type, value }] of Object.entries(perk.stats)) {
      result.push({
        stat: stat as keyof BotStats,
        type,
        value,
      });
    }
  }

  // Sort adjustments so that absolute ones apply first, then percentage basd
  // ones.
  return result.sort((a, b) => (a.type < b.type ? -1 : 1));
}

export function applyPerkStatAdjustments(
  base: BotStats,
  perks: readonly Perk[],
): BotStats {
  // Create a writable stats object.
  let stats: { -readonly [K in keyof BotStats]: BotStats[K] } = { ...base };

  const combine = (
    stat: keyof BotStats,
    adjustment: PerkStatAdjustment<keyof BotStats>,
  ) => {
    return adjustment.type === 'absolute'
      ? stats[stat] + adjustment.value
      : stats[stat] * (1 + adjustment.value);
  };

  for (const adjustment of preparePerkStatAdjustments(perks)) {
    stats[adjustment.stat] = combine(adjustment.stat, adjustment);
  }

  // Stats that should be rounded.
  const rounded: readonly (keyof BotStats)[] = [
    'clipSize',
    'damage',
    'durability',
    'fireCooldownMs',
    'reloadCooldownMs',
  ];

  for (const stat of rounded) {
    stats[stat] = Math.round(stats[stat]);
  }

  return stats;
}

export function getAllPerks(): readonly Perk[] {
  return Array.from(perks.values());
}

register(new AITargetingPerk());
register(new FortifiedPerk());
register(new LaserPerk());
register(new ReflectPerk());
register(new RegenerationPerk());
register(new SplitShotPerk());
register(new DeepPocketsPerk());
register(new ExplosiveEndPerk());
register(new BurstPerk());
