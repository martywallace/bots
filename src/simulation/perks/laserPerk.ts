import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export class LaserPerk implements Perk {
  public readonly id = PerkID.Laser;

  public readonly metadata = {
    name: 'Lasers',
    description: ({ stats }: Perk) =>
      `Removes reload at the cost of ${(Math.abs(stats.damage!.value) * 100).toFixed(0)}% damage.`,
    reason: 'Part of the Telecom sector.',
    icon: faBolt,
  };

  public readonly settings = {};

  public readonly stats: PerkStatAdjustments = {
    damage: { type: 'percent', value: -0.25 },
    clipSize: { type: 'absolute', value: Infinity },
  };

  public valid(company: Company): boolean {
    return company.sector.name === 'Communication Services';
  }
}
