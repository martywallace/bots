import { faBackward } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export type ReflectPerkSettings = {
  readonly chance: number;
};

export class ReflectPerk implements Perk<ReflectPerkSettings> {
  public readonly id = PerkID.Reflect;

  public readonly metadata = {
    name: 'Reflect',
    description: ({ settings, stats }: Perk<ReflectPerkSettings>) =>
      `${(settings.chance * 100).toFixed(
        0,
      )}% chance to reflect incoming projectiles at the cost of ${Math.abs(
        stats.durability!.value * 100,
      ).toFixed()}% durability`,
    reason: 'Dividend score is 3 or more.',
    icon: faBackward,
  };

  public readonly settings: ReflectPerkSettings = {
    chance: 0.3,
  };

  public readonly stats: PerkStatAdjustments = {
    durability: { type: 'percent', value: -0.2 },
  };

  public valid(company: Company): boolean {
    return company.score.dividend >= 3;
  }
}
