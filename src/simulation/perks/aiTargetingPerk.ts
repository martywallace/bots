import { faRobot } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export class AITargetingPerk implements Perk {
  public readonly id = PerkID.AITargeting;

  public readonly metadata = {
    name: 'AI Targeting',
    description: ({ stats }: Perk) =>
      `Improves accuracy by ${(
        (Math.abs(stats.accuracyAngle!.value) / Math.PI) *
        200
      ).toFixed(1)}%.`,
    reason: 'Part of the Tech sector.',
    icon: faRobot,
  };

  public readonly settings = {};

  public readonly stats: PerkStatAdjustments = {
    accuracyAngle: { type: 'absolute', value: -Math.PI / 180 },
  };

  public valid(company: Company): boolean {
    return company.sector.name === 'Information Technology';
  }
}
