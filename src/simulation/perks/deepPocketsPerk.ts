import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export class DeepPocketsPerk implements Perk {
  public readonly id = PerkID.DeepPockets;

  public readonly metadata = {
    name: 'Deep Pockets',
    description: ({ stats }: Perk) =>
      `Grants ${(stats.clipSize!.value * 100).toFixed(
        0,
      )}% additional ammo.`,
    reason: 'Part of the Financial sector.',
    icon: faCoins,
  };

  public readonly settings = {};

  public readonly stats: PerkStatAdjustments = {
    clipSize: { type: 'percent', value: 0.5 },
  };

  public valid(company: Company): boolean {
    return company.sector.name === 'Financials';
  }
}
