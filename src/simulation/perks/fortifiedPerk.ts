import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export class FortifiedPerk implements Perk {
  public readonly id = PerkID.Fortified;

  public readonly metadata = {
    name: 'Fortified',
    description: ({ stats }: Perk) =>
      `Grants ${(stats.durability!.value * 100).toFixed(
        0,
      )}% additional durability.`,
    reason: 'Health score is 3 or more.',
    icon: faShieldAlt,
  };

  public readonly settings = {};

  public readonly stats: PerkStatAdjustments = {
    durability: { type: 'percent', value: 0.3 },
  };

  public valid(company: Company): boolean {
    return company.score.health >= 3;
  }
}
