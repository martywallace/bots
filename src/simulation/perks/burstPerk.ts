import { faCoins, faFastForward } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export class BurstPerk implements Perk {
  public readonly id = PerkID.Burst;

  public readonly metadata = {
    name: 'Burst Fire',
    description: ({ stats }: Perk) =>
      `Reduces the cooldown between shots by ${(
        Math.abs(stats.fireCooldownMs!.value) * 100
      ).toFixed(0)}% while  doubling reload time and halving damage.`,
    reason: 'Future score is over 3.',
    icon: faFastForward,
  };

  public readonly settings = {};

  public readonly stats: PerkStatAdjustments = {
    fireCooldownMs: { type: 'percent', value: -0.75 },
    reloadCooldownMs: { type: 'percent', value: 1 },
    damage: { type: 'percent', value: -0.5 },
  };

  public valid(company: Company): boolean {
    return company.score.future > 3;
  }
}
