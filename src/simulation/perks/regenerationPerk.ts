import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export type RegenerationPerkSettings = {
  readonly percentage: number;
  readonly rate: number;
};

export class RegenerationPerk implements Perk<RegenerationPerkSettings> {
  public readonly id = PerkID.Regeneration;

  public readonly metadata = {
    name: 'Regeneration',
    description: ({ settings, stats }: Perk<RegenerationPerkSettings>) =>
      `Regenerates ${(settings.percentage * 100).toFixed(
        0,
      )}% durability every ${(settings.rate / 1000).toFixed()}s.`,
    reason: 'Part of the Healthcare sector.',
    icon: faHeart,
  };

  public readonly settings: RegenerationPerkSettings = {
    percentage: 0.05,
    rate: 5000,
  };

  public readonly stats: PerkStatAdjustments = {};

  public valid(company: Company): boolean {
    return company.sector.name === 'Healthcare';
  }
}
