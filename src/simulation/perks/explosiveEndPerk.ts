import { faBomb } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export type ExplosiveEndPerkSettings = {
  readonly minBullets: number;
  readonly maxBullets: number;
};

export class ExplosiveEndPerk implements Perk<ExplosiveEndPerkSettings> {
  public readonly id = PerkID.ExplosiveEnd;

  public readonly metadata = {
    name: 'Explosive End',
    description: ({ settings }: Perk<ExplosiveEndPerkSettings>) =>
      `Explode in a hail of ${settings.minBullets} to ${settings.maxBullets} bullets on death.`,
    reason: 'Past score is 4 or more.',
    icon: faBomb,
  };

  public readonly settings: ExplosiveEndPerkSettings = {
    minBullets: 40,
    maxBullets: 60,
  };

  public readonly stats: PerkStatAdjustments = {
  };

  public valid(company: Company): boolean {
    return company.score.past >= 4;
  }
}
