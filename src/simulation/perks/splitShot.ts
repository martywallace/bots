import { faAdjust } from '@fortawesome/free-solid-svg-icons';
import { Perk, PerkID, PerkStatAdjustments } from '.';
import { Company } from '../../api/queries/getCompany';

export type SplitShotPerkSettings = {
  readonly chance: number;
  readonly bullets: number;
};

export class SplitShotPerk implements Perk<SplitShotPerkSettings> {
  public readonly id = PerkID.SplitShot;

  public readonly metadata = {
    name: 'Split Shot',
    description: ({ settings, stats }: Perk<SplitShotPerkSettings>) =>
      `${(settings.chance * 100).toFixed(
        0,
      )}% chance that projectiles will split into ${
        settings.bullets
      }. Increases maximum ammunition by ${stats.clipSize!.value}.`,
    reason: 'Value score is 3 or more.',
    icon: faAdjust,
  };

  public readonly settings: SplitShotPerkSettings = {
    chance: 0.35,
    bullets: 3,
  };

  public readonly stats: PerkStatAdjustments = {
    clipSize: { type: 'absolute', value: 10 },
  };

  public valid(company: Company): boolean {
    return company.score.value >= 3;
  }
}
