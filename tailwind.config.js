module.exports = {
  purge: ['./src/**/*.{ts,tsx,js,jsx}'],
  content: [],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Raleway', 'sans-serif'],
      },
      colors: {
        primary: {
          DEFAULT: '#9CFFFA',
        },
        good: {
          DEFAULT: '#7FB257',
        },
        bad: {
          DEFAULT: '#EE4266',
        },
        ui: {
          lightest: '#868dac',
          lighter: '#6d779c',
          light: '#5b6386',
          DEFAULT: '#4A516D',
          dark: '#424861',
          darker: '#3a3f55',
          darkest: '#323649',
        },
      },
    },
    typography: (theme) => ({
      DEFAULT: {
        css: {
          lineHeight: 1.6,
          color: theme('colors.ui.lightest'),
        },
      },
    }),
  },
  plugins: [require('@tailwindcss/typography')],
};
